// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyC0W5ymxyybzMHjkcpEcCMpj7h8l5jhyss",
    authDomain: "todolist-db7bf.firebaseapp.com",
    databaseURL: "https://todolist-db7bf.firebaseio.com",
    projectId: "todolist-db7bf",
    storageBucket: "todolist-db7bf.appspot.com",
    messagingSenderId: "945676587778",
    appId: "1:945676587778:web:8b290c5d279a7b4b1e281b",
    measurementId: "G-GQ1BT4HSPF"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
