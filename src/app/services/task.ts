export class Task {
    id: string;
    id_projet:string;
    titre: string;
    description: string;
    categorie: string;
    priorite: string;
    type: string;
    date_debut: string;
    date_fin: string;
    rappel: string;
    etat: string;
    user_id:string;
}
