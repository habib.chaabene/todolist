import { UsersService } from './user.service';
import { Router } from '@angular/router';
import { User } from './user';
import { BehaviorSubject } from 'rxjs';
import { Injectable } from "@angular/core";
import * as firebase from 'firebase/app';
import 'firebase/auth';
import { AngularFirestore, AngularFirestoreDocument, DocumentReference } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { ReplaySubject } from 'rxjs';

@Injectable()
export class AuthService {
  public currentUser: any = null;
  userData: any;
  userInfo: any;
  grade: any;
  gr: string;
  userid: any;
  users: User[];
  constructor(
    public afs: AngularFirestore,   // Inject Firestore service
    public afAuth: AngularFireAuth,
    private service: UsersService,
    private router: Router
  ){
    this.afAuth.authState.subscribe(user => {
      if (user) {
        // this.UserRef = this.api.GetUser(id);
        const userSubject = new ReplaySubject(1);
        userSubject.next(user.email);
        this.userData = user;
        /*
        const medecins =  this.afs.collection('medecins', ref => ref.where('uid', '==', user.uid));
        const agents =  this.afs.collection('agents', ref => ref.where('uid', '==', user.uid));
        const users =  this.afs.collection('users', ref => ref.where('uid', '==', user.uid));
        


        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));

        console.log(this.userData.uid);
        this.userid = this.userData.uid;
        */
      }

    });
  }
  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.router.navigate(['/type-user']);
  }
  login(value){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(value.email, value.password)
      .then(
        res => {
          resolve(res);
          this.loginUser(res.user.uid);
          console.log(res);
        },
        err => reject(err))
    })
  }
  loginUser(value){
    this.service.getUser().subscribe(Patient => {
      this.users = Patient.map(item => {
        let uid = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { uid, ...(data as {}) } as User;
      });
      //console.log(value);
      this.users.forEach(x => {
        console.log(x.uid);
      	if(x.uid == value)
      	{
      		localStorage.setItem('userInfo', JSON.stringify(x));
      		console.log('user',x);
          this.router.navigate(['/home']);
      	}
           
        });      	

    });
  }

   register(value){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
      .then((result) => {
        console.log(result.user);

        this.addUser(value,result.user.uid);
        this.router.navigate(['/login']);
      }).catch((error) => {
        console.log(error);
      });
   });
  }

  public addUser(user: any, id) {
    return this.afs.collection('users')
    .doc(id).set({
      ...user
    });
  }
  
  resetPassword(email:string): Promise<void> {
    return firebase.auth().sendPasswordResetEmail(email);
  }
}
