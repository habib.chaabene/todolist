export class Enfant {
    uid: string;
    nom: string;
    prenom: string;
    email: string;
    password: string;
    photo: string;
    sexe: string;
}