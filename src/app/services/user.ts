export class User {
    uid: string;
    nom: string;
    prenom: string;
    adresse: string;
    email: string;
    password: string;
    telephone: number;
    id: string;
    name: string;
    picture: {
        data: {
            url: string;
        };
    };
}