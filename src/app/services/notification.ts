export class Notification {
    uid: string;
    nom: string;
    date_debut:string;
    date_fin:string;
    user_id: number;
}