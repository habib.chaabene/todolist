import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Projet } from './projet';

@Injectable({
  providedIn: 'root'
})
export class ProjetService {

  Projet: any;
  user: any;
  
  constructor(private afs: AngularFirestore) {  
    this.user = JSON.parse(localStorage.getItem('userInfo'));   
  }

  public createProjet(projet: Projet): Promise<DocumentReference> {
    return this.afs.collection('projets').add({
      ...projet
    });
  }

  getProjets () {
    return this.afs.collection('projets', ref => ref.where('user_id', '==', this.user.uid)).snapshotChanges();
  }
  public updateProjet(projet: Projet,id) {
    //console.log(projet);
    return this.afs.collection('projets').doc(id).update(projet);
  }
  public deleteprojet(id){
    return this.afs.collection('projets').doc(id).delete();
  }
}
