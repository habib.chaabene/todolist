import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Categorie } from './categorie';

@Injectable({
  providedIn: 'root'
})
export class CategorieService {

  Categorie: any;
  
  constructor(private afs: AngularFirestore) {     
  }

  public createCategorie(categorie: Categorie): Promise<DocumentReference> {
    return this.afs.collection('categories').add({
      ...categorie
    });
  }

  getCategories () {
    return this.afs.collection('categories').snapshotChanges();
  }
  public updateCategorie(categorie: Categorie,id) {
    //console.log(categorie);
    return this.afs.collection('categories').doc(id).update(categorie);
  }
  public deletecategorie(id){
    return this.afs.collection('categories').doc(id).delete();
  }
}
