import { TokenService } from './token.service';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Task } from './task';
import { DocumentReference, AngularFirestore } from '@angular/fire/firestore';

/*
let headers = new Headers();
headers.append('Content-Type', 'application/json');
headers.append('authentication', `${TokenService.get}`);

let options = new RequestOptions({ headers: headers });
*/

@Injectable({
  providedIn: 'root'
})
export class TacheEnfantService {
  Task: any;
  user: any;
  
  constructor(private afs: AngularFirestore) { 
    this.user = JSON.parse(localStorage.getItem('userInfo'));
  }

  public createTask(task: Task): Promise<DocumentReference> {
    return this.afs.collection('taches').add({
      ...task
    });
  }
  public createTaskenfant(task){
    return this.afs.collection('taches-enfants').add(task);
  }
  
  getTasks () {
    return this.afs.collection('taches', ref => ref.where('user_id', '==', this.user.uid)).snapshotChanges();
  }

  getTasksenfant () {
    return this.afs.collection('taches-enfants', ref => ref.where('user_id', '==', this.user.uid)).snapshotChanges();
  }

  getTaskscomplete(){
    return this.afs.collection('taches', ref => ref.where('etat', '==', 'terminer')).snapshotChanges();
  }

  getTasksongoing(){
    return this.afs.collection('taches', ref => ref.where('etat', '==', 'encour')).snapshotChanges();
  }

  public updateTask(task: Task) {
    console.log(task);
    return this.afs.collection('taches').doc(task.id).update(task);
  }
  public deletetask(id){
    return this.afs.collection('taches').doc(id).delete();
  }
  public updatetacheenfant(task: Task) {
    console.log(task);
    return this.afs.collection('taches-enfants').doc(task.id).update(task);
  }
  public deletetache(id){
    return this.afs.collection('taches-enfants').doc(id).delete();
  }
}
