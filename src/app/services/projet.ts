export class Projet {
    id: string;
    titre: string;
    description: string;
    progression: string;
    date_debut: string;
    date_fin: string;
    user_id:string;
}
