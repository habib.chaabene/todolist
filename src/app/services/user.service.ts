import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class UsersService {


  constructor(private afs: AngularFirestore) { 
    
  }
 
  getUser () {
    return this.afs.collection('users').snapshotChanges();
  }

  getEnfant () {
    return this.afs.collection('users-enfants').snapshotChanges();
  }
}