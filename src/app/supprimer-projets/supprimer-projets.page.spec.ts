import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimerProjetsPage } from './supprimer-projets.page';

describe('SupprimerProjetsPage', () => {
  let component: SupprimerProjetsPage;
  let fixture: ComponentFixture<SupprimerProjetsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupprimerProjetsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimerProjetsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
