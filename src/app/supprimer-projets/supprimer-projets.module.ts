import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SupprimerProjetsPage } from './supprimer-projets.page';

const routes: Routes = [
  {
    path: '',
    component: SupprimerProjetsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SupprimerProjetsPage]
})
export class SupprimerProjetsPageModule {}
