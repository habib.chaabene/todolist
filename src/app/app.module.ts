
import { GiftPageModule } from './gift/gift.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { IonicStorageModule } from '@ionic/storage';

//import { MenuComponentComponent } from'./components/menu-component/menu-component.component';
import { Camera } from '@ionic-native/camera/ngx'; 
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Facebook } from '@ionic-native/facebook/ngx';
import { AuthService } from './services/auth.service';
import { environment } from 'src/environments/environment';
//  firebase imports, remove what you don't require
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { VideoPlayer } from '@ionic-native/video-player/ngx';
import { CalendarModule } from 'ion2-calendar';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ViewVideoPageModule } from './view-video/view-video.module';
import { TextToSpeech } from '@ionic-native/text-to-speech/ngx'
// Import ng-circle-progress

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    AppRoutingModule,    
    FormsModule,
    ReactiveFormsModule,
    ViewVideoPageModule,
    GiftPageModule,
    CalendarModule
    // Specify ng-circle-progress as an import     
  ],
  providers: [
    
    VideoPlayer,
    StatusBar,
    Camera,
    SplashScreen,
    AuthService,  
    InAppBrowser,
    LocalNotifications,  
    Facebook,
    SocialSharing,
    TextToSpeech,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
