import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AujourdhuiPage } from './aujourdhui.page';

describe('AujourdhuiPage', () => {
  let component: AujourdhuiPage;
  let fixture: ComponentFixture<AujourdhuiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AujourdhuiPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AujourdhuiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
