import { TaskService } from './../services/task.service';
import { Component, OnInit } from '@angular/core';
import { Task } from '../services/task';
import { trigger, state, style } from '@angular/animations';

@Component({
  selector: 'app-aujourdhui',
  templateUrl: './aujourdhui.page.html',
  styleUrls: ['./aujourdhui.page.scss'],
  //for animation purpose
  animations: [
    trigger('itemState', [
      state('idle', style({
        opacity: '0.3',
        transform: 'scale(1)'
      })),
    ])
  ]
})
export class AujourdhuiPage implements OnInit {

  user: any;
  taches: any[];

  constructor(private taskservice: TaskService) {
    
   }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('userInfo'));
     //this.type = this.user.user.type;
      console.log(this.user);
    this.taskservice.getTasks(this.user.uid).subscribe(rdvs => {

      this.taches = rdvs.map(item => {
        console.log(item);
        let id = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { id, ...(data as {}) } as Task;
      });
    });
    
  }

}
