import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListesUtilisateursPage } from './listes-utilisateurs.page';

const routes: Routes = [
  {
    path: '',
    component: ListesUtilisateursPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListesUtilisateursPage]
})
export class ListesUtilisateursPageModule {}
