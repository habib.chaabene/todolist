import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListesUtilisateursPage } from './listes-utilisateurs.page';

describe('ListesUtilisateursPage', () => {
  let component: ListesUtilisateursPage;
  let fixture: ComponentFixture<ListesUtilisateursPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListesUtilisateursPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListesUtilisateursPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
