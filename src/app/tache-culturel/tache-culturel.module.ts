import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TacheCulturelPage } from './tache-culturel.page';

const routes: Routes = [
  {
    path: '',
    component: TacheCulturelPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TacheCulturelPage]
})
export class TacheCulturelPageModule {}
