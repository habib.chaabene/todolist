import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TacheCulturelPage } from './tache-culturel.page';

describe('TacheCulturelPage', () => {
  let component: TacheCulturelPage;
  let fixture: ComponentFixture<TacheCulturelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TacheCulturelPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TacheCulturelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
