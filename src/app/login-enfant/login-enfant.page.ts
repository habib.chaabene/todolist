import { Enfant } from './../services/enfant';
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { Platform, MenuController, NavController, LoadingController, AlertController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { UsersService } from '../services/user.service';

@Component({
  selector: 'app-login-enfant',
  templateUrl: './login-enfant.page.html',
  styleUrls: ['./login-enfant.page.scss'],
})
export class LoginEnfantPage implements OnInit {

  loginForm: FormGroup;
  errorMessage: string = '';
  private backButtonSub: Subscription;
  userProfile: any = null;
 
  isLoggedIn = false;
  
  loading: any;
  users: Enfant[];
 
  constructor(

    private platform: Platform,
    private menuCtrl: MenuController,
    private navCtrl: NavController,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    public loadingController: LoadingController,
    private fireAuth: AngularFireAuth,
    private alertCtrl: AlertController,
    private router: Router,
    private afs: AngularFirestore,
    private fb: Facebook,
    public afAuth: AngularFireAuth,
    private service: UsersService,
 
  ) { 
    
  }
 
  async ngOnInit() {
    //this.menuCtrl.enable(false); // or true
 
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
    
    this.loading = await this.loadingController.create({
      message: 'Connecting ...'
    });
  }
  async presentLoading(loading) {
    await loading.present();
  }

  
  

  login(){
    this.service.getEnfant().subscribe(Patient => {
      this.users = Patient.map(item => {
        let uid = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { uid, ...(data as {}) } as Enfant;
      });
      //console.log(value);
      this.users.forEach(x => {
        console.log(this.loginForm.value.email,this.loginForm.value.password);
      	if((x.email == this.loginForm.value.email) && (x.password == this.loginForm.value.password))
      	{
      		localStorage.setItem('userInfo', JSON.stringify(x));
      		console.log('user',x);
          this.router.navigate(['/accueil-enfants']);
      	}           
        });      	

    });
  }
 
  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };
 
 
  ionViewDidEnter() {
    this.backButtonSub = this.platform.backButton.subscribeWithPriority(
      10000,
      () => this.onBack()
    );
  }

  ionViewWillLeave() {
    this.backButtonSub.unsubscribe();
  }

  private async onBack() {
    const openMenu = await this.menuCtrl.getOpen();
    if (openMenu) {
      await openMenu.close();
    } else {
      await this.navCtrl.pop();
    }
  }
 
}
