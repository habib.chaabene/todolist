import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { VideoPlayer } from '@ionic-native/video-player/ngx';
import { MenuController, ModalController } from '@ionic/angular';
import { ViewVideoPage } from '../view-video/view-video.page';

@Component({
    selector: 'app-tab-chat',
    templateUrl: './tab-chat.page.html',
    styleUrls: ['./tab-chat.page.scss'],
})
export class TabChatPage implements OnInit {
    public items = [
        { text: "مهام  جاهزة  ", short_des: "It's an amazing tonigth", img: "assets/images/Pictures/faces-images/face_image1.png" },
        { text: "مهام  شخصية", short_des: "Good Morning, Have a nice day ", img: "assets/images/Pictures/faces-images/face_image2.png" },
        { text: "مهام  منزلية", short_des: "Hye Maria, How's you ? ", img: "assets/images/Pictures/faces-images/face_image3.png" },
        { text: "مهام  تثقيفية", short_des: "Hey Dude, What's going on ", img: "assets/images/Pictures/faces-images/face_image4.png" },
        { text: " مهام أخرى ", short_des: " It's happines you are good ", img: "assets/images/Pictures/faces-images/face_image5.png" }
       
    ];
    videourl: string;

  constructor(private authService:AuthService,
    public menuCtrl: MenuController,
    private videoPlayer: VideoPlayer, public modalCtrl: ModalController) {
  }
  async viewVideo(urls,gift) {
    const modal = await this.modalCtrl.create({
      component: ViewVideoPage,
      componentProps: { url: urls,gift:gift },
      cssClass: 'viewVideoModal'
    })
    return modal.present();
  }

  
  ngOnInit() {
    this.menuCtrl.enable(false, 'Menu2')
    this.menuCtrl.enable(false, 'Menu1')
    //this.menuCtrl.swipeEnable(false);
  }
  logout(){
    this.authService.logout();
  }
  profile(){}

}