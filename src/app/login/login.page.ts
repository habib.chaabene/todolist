import { UsersService } from './../services/user.service';
import { NavController, MenuController, Platform, AlertController, LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import * as firebase from 'firebase';
import 'firebase/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from '../services/user';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  errorMessage: string = '';
  private backButtonSub: Subscription;
  userProfile: any = null;
 
  isLoggedIn = false;
  loading: any;
  users: User[];
  clickSub: any;
 
  constructor(
    private service: UsersService,
    private platform: Platform,
    private menuCtrl: MenuController,
    private navCtrl: NavController,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    public loadingController: LoadingController,
    private fireAuth: AngularFireAuth,
    private alertCtrl: AlertController,
    private router: Router,
    private afs: AngularFirestore,
    private fb: Facebook,
    private localNotifications: LocalNotifications,
    public alertController: AlertController
 
  ) { 
    
    
    fb.getLoginStatus()
    .then(res => {
      console.log(res.status);
      if (res.status === 'connect') {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    })
    .catch(e => console.log(e));
  }
 
  async ngOnInit() {
    this.menuCtrl.enable(false, 'custom');
    //this.simpleNotif();
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
    
    this.loading = await this.loadingController.create({
      message: 'Connecting ...'
    });
  }
  async presentLoading(loading) {
    await loading.present();
  }
  async presentAlert(data) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: data,
      buttons: ['OK']
    });

    await alert.present();
  }
  unsub() {
    this.clickSub.unsubscribe();
  }
  simpleNotif() {
    this.clickSub = this.localNotifications.on('click').subscribe(data => {
      console.log(data);
      // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
      this.presentAlert('Your notifiations contains a secret = ' + data.data.secret);
      this.unsub();
    });
    this.localNotifications.schedule({
      id: 1,
      text: 'Single Local Notification',
      data: { secret: 'secret' }
    });

  }

  async fbLogin() {
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then(res => {
        if (res.status === 'connected') {
          this.isLoggedIn = true;
          //this.onLoginSuccess(res);
          //console.log('authResponse',res.authResponse);
          this.getUserDetail(res.authResponse.userID);
          
          //this.navCtrl.navigateForward('/home');
          //console.log(res.authResponse);
          this.onLoginSuccess(res);
        } else {
          this.isLoggedIn = false;
          alert(this.isLoggedIn);
        }
      })
      .catch(e => {
        console.log('Error logging into Facebook', e);
        alert('Error logging into Facebook'+e);
      }); 
  }
  onLoginSuccess(res: FacebookLoginResponse) {
    // const { token, secret } = res;
    const credential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
    this.fireAuth.auth.signInWithCredential(credential)
      .then((response) => {
        console.log('response',response);
        this.afs.collection('users').doc(response.user.uid).set(this.users);
        this.loginUser(response.user.uid);
        this.loading.dismiss();
      })

  }
  onLoginError(err) {
    console.log(err);
  }
  getUserDetail(userid: any) {
    this.fb.api('/' + userid + '/?fields=id,email,name,picture', ['public_profile'])
      .then(res => {
        console.log(res);
        this.users = res;
      })
      .catch(e => {
        console.log(e);
      });
  }
  logout() {
    this.fb.logout()
      .then( res => this.isLoggedIn = false)
      .catch(e => console.log('Error logout from Facebook', e));
  }
  

  login(){
    this.authService.login(this.loginForm.value);
  }

  loginUser(value){
    this.service.getUser().subscribe(Patient => {
      this.users = Patient.map(item => {
        let uid = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { uid, ...(data as {}) } as User;
      });
      console.log(value);
      this.users.forEach(x => {
      	if(x.uid == value)
      	{
      		localStorage.setItem('userInfo', JSON.stringify(x));
      		console.log('user',x);
          this.router.navigate(['/home']);
      	}           
        });      	

    });
  }
  
 
  
  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };
 
 
  ionViewDidEnter() {
    this.backButtonSub = this.platform.backButton.subscribeWithPriority(
      10000,
      () => this.onBack()
    );
  }

  ionViewWillLeave() {
    this.backButtonSub.unsubscribe();
  }

  private async onBack() {
    const openMenu = await this.menuCtrl.getOpen();
    if (openMenu) {
      await openMenu.close();
    } else {
      await this.navCtrl.pop();
    }
  }
 
  resetPassword() {
    this.router.navigateByUrl('form-forget-three');
  }
  
  
}
