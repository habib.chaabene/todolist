import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterTachePage } from './ajouter-tache.page';
import { ReactiveFormsModule } from '@angular/forms';

describe('AjouterTachePage', () => {
  let component: AjouterTachePage;
  let fixture: ComponentFixture<AjouterTachePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterTachePage ],
      imports: [ReactiveFormsModule],  
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterTachePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
