import { AngularFirestore } from '@angular/fire/firestore';
import { Component, OnInit, LOCALE_ID } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ModalController, NavController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { mobiscroll, MbscModule  } from '@mobiscroll/angular-lite';
import { TaskService } from '../services/task.service';
import { Projet } from '../services/projet';
import { ProjetService } from '../services/projet.service';
import { DatePipe } from '@angular/common';
import { Categorie } from '../services/categorie';
import { Task } from '../services/task';
//import { MbscDatetimeOptions } from '../lib/mobiscroll/js/mobiscroll.angular.min.js';
mobiscroll.settings = {
    theme: 'ios',
    themeVariant: 'light',
    display: 'bubble'
};
const now = new Date();
@Component({
  selector: 'app-ajouter-tache',
  templateUrl: './ajouter-tache.page.html',
  styleUrls: ['./ajouter-tache.page.scss'],
  providers: [DatePipe,{ provide: LOCALE_ID, useValue: "fr-FR" }]
})
export class AjouterTachePage implements OnInit {

  modalTitle: string;
  modelId: number;
  external = now;
  tacheForm: FormGroup;
  progression: number;
  projets: Projet[];
  user: any;
  debut: string;
  fin: string;
  categories: Categorie[];
  taches: any;
  nbr: any;
  date: Date;
  today: string;
  min: string;
 
  constructor(private projetservice: ProjetService,
    private modalController: ModalController,
    public toastCtrl: ToastController,
    public fb: FormBuilder,
    private datePipe: DatePipe,
    private navCtrl: NavController,
    private router: Router,
    private taskService:TaskService,
    private firestore: AngularFirestore
  ) { }
    
  externalSettings: MbscModule  = {
    controls: ['calendar', 'time'],
    showOnTap: false,
    showOnFocus: false
};

  ngOnInit() {
    this.date = new Date();
    let maxDate: any = new Date(new Date().setFullYear(new Date().getFullYear() + 2)).toISOString();
    this.today = this.datePipe.transform(this.date, 'yyyy-MM-dd');
    let jour = new Date(this.today);
    this.user=JSON.parse(localStorage.getItem('userInfo'));
    console.log('user',this.today);
    this.tacheForm = this.fb.group({
      nom: [''],
      commentaire: [''],
      categorie: [''],
      priorite: [''],
      date_debut: [''],
      date_fin: [''],
      rappel: [''],
      repetition: [''],
      progression:[''],
      projet:[''],
      user_id:[this.user.uid],
      etat:['encour']
    });
    this.projetservice.getProjets().subscribe(rdvs => {

      this.projets = rdvs.map(item => {
       // console.log(item);
        let id = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { id, ...(data as {}) } as Projet;
      });
      console.log('projets',this.projets );
    });
    this.firestore.collection('categories', ref => ref.where('user_id', '==', this.user.uid)).snapshotChanges().subscribe(rdvs => {
      this.categories = rdvs.map(item => {
       // console.log(item);
        let uid = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { uid, ...(data as {}) } as Categorie;
      });
      console.log(this.categories );
    });
  }
  change(ev){
    this.min = this.datePipe.transform(ev.detail.value, 'yyyy-MM-dd');
    console.log('min',this.min);
    console.log(ev.detail.value);
  }
  async openToast() {   
    const toast = await this.toastCtrl.create({  
      message: 'tache ajouté avec succés ',  
      animated: false,  
      duration: 8000,
        
      position: 'middle',  
    });  
    toast.present();  
    toast.onDidDismiss().then((val) => {  
      console.log('Toast Dismissed');   
    });  
  } 
  formSubmit() {
    if (!this.tacheForm.valid) {
      return false;
    } else {
      //this.tacheForm.progression = 0;
      console.log(this.tacheForm.value);
       this.progression = 0;
       this.debut = this.datePipe.transform(this.tacheForm.value.date_debut, 'yyyy-M-d HH:mm');
       this.fin = this.datePipe.transform(this.tacheForm.value.date_fin, 'yyyy-M-d HH:mm');
       this.tacheForm.value.date_debut= this.debut;
       this.tacheForm.value.date_fin = this.fin;
       this.getprog(this.tacheForm.value.projet);
      this.taskService.createTask(this.tacheForm.value).then(res => {
        console.log(res);
        this.tacheForm.reset();
        this.openToast();
        this.router.navigate(['/home']);
      },error => console.log(error));
       // .catch(error => console.log(error));       
    }
  }
  getprog(id){
    this.taskService.getTasksbyprojet(id).subscribe(rdvs => {

      this.taches = rdvs.map(item => {
        console.log(item);
        let id = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { id, ...(data as {}) } as Task;
      });
      let i =0;
      this.taches.forEach(x=>{
        if(x.etat == 'terminer')
        i++;
      })
      console.log(this.taches.length,i);
      this.nbr = this.taches.length;
      this.progression = i/ this.nbr;
      let record ={};
      record['progression'] = this.progression;
      this.taskService.updateProg(id,record);
    });

  }
 
  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }

  goback(){
    this.navCtrl.back();
  }
  minDate: Date = new Date(2017, 11, 1);
    maxDate: Date = new Date(2017, 11, 10);

    myDateOptions: MbscModule = {
        display: 'top',
        theme: 'ios',
        max: new Date(),
        min: new Date(1960, 0, 1)
    };

    birthday: Date;
    appointment: Array<Date> = [];

}
