import { User } from './../services/user';
import { Enfant } from './../services/enfant';
import { Component, LOCALE_ID, OnInit } from '@angular/core';
import { CustomThemeService } from '../services/custom-theme.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AlertController, MenuController, NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AngularFireStorage} from '@angular/fire/storage'; 
import * as firebase from 'firebase/app'; 
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-step-one',
  templateUrl: './step-one.page.html',
  styleUrls: ['./step-one.page.scss'],
  providers: [DatePipe,{ provide: LOCALE_ID, useValue: "fr-FR" }]
})
export class StepOnePage implements OnInit {
  registerForm: FormGroup;
  errorMessage: string = '';
  state = false;
  type ="selected";
  type1 ="unselected";


  //formulario para el registro del empleado
  ///foto seleccionada para envio a almacenar
  selectedPhoto;
  ///foto seleccionada a mostrar en imagen capturada
  selectedPhotobase64="/assets/img/camera-icon.png";  
  nbr: any;
  users: User[];
  verif: boolean;
  min: string;
  max: string;
  ///set imports
  
  constructor(
    private menuCtrl: MenuController,
    private navCtrl: NavController,
    private camera: Camera,
    private alertCtrl: AlertController,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private afs: AngularFirestore,
    private router: Router
 
  ) { }
 
  ngOnInit() {
    let d = new Date();
    let year = d.getFullYear();
    let month = d.getMonth();
    let day = d.getDate();
    let c = new Date(year - 4, month, day);
    let dx = new Date(year - 11, month, day);
    console.log(c);
    this.max = this.datePipe.transform(c, 'yyyy-MM-dd');
    this.min = this.datePipe.transform(dx, 'yyyy-MM-dd');
    //this.menuCtrl.enable(false); // or true
    console.log(this.min,this.max);
    this.registerForm = this.formBuilder.group({
      nom:new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[a-zA-Z]+$')
      ])),
      prenom:new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z]+$')
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
      date_naissance:new FormControl(''),
      sexe:[''],
    });
  }
  
 

  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };
 
  radioGroupChange($event){
    console.log($event.target.value);
  }
 
  registerUser(){
    console.log(this.registerForm.value);
    
    this.afs.collection('users-enfants').add(this.registerForm.value)
    .then(res => {
      console.log(res.id);
      this.errorMessage = "";
      localStorage.setItem('enfant_id',res.id)
      localStorage.setItem('enfant',JSON.stringify(this.registerForm.value))
      this.navCtrl.navigateForward('/login-enfant');
    }, err => {
      this.errorMessage = err.message;
    })
    
  }
  subirfoto(selectedPhoto){ 
    return firebase.storage().ref().child(`enfants/enfant_${ new Date().getTime() }.jpg`).put(selectedPhoto); 
  }
  checkEmail(email){
    console.log(email.value);
    this.afs.collection('users', ref => ref.where('email', '==', email.value)).snapshotChanges().subscribe(rdvs => {

      this.users = rdvs.map(item => {
       // console.log(item);
        let uid = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { uid, ...(data as {}) } as User;
      });
      this.nbr = this.users.length;
      if(this.users.length == 0){
        this.verif= true;
      }
      if(this.users.length == 1){
        this.verif= false;
      }
      console.log(this.users.length );
    });
  }
 
  goToRegisterPage(){
    this.navCtrl.navigateForward('/form-register-three');
  }
  
  tomarfoto() { 
    ///opciones de la camara
    const options: CameraOptions = {
      //calidad
      quality: 100,
      ////dimensiones de la fotografia
      targetHeight: 100,
      targetWidth: 100,
      ///tipo de destino
      destinationType: this.camera.DestinationType.DATA_URL,
      ///tipo de foto
      encodingType: this.camera.EncodingType.JPEG,
      //definir que es una imagen
      mediaType: this.camera.MediaType.PICTURE
    }
    //obtener imagen capturada
    this.camera.getPicture(options).then((imageData) => {  
      ////obtener la foto
      this.selectedPhoto  = this.dataURItoBlob('data:image/jpeg;base64,' + imageData); 
      this.selectedPhotobase64='data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log('error', err);
    });
  }   
  ///function to create Employee
  async createEnfant() { 
    //creo un objeto tipo Empleado para enviar a registro
    let empleado: Enfant={  
      nom: this.registerForm.value.nom,
      email: this.registerForm.value.email,
      prenom: this.registerForm.value.prenom,
      photo: this.selectedPhoto,
      sexe: this.registerForm.value.sexe,
      password: this.registerForm.value.email,
      uid: '',
    };  
    ///the photo is sent to store it
    const subida=this.subirfoto(this.selectedPhoto)
    //expected to upload the photograph
    await subida.then(snapshot => { 
      snapshot.ref.getDownloadURL().then(async url => {  
        ///the url of the stored photograph is obtained
        empleado.photo=url;
        ///I send the information to the registry
        await this.afs.collection('users-enfants').add(empleado).then(async result => { 
         
          ///se reinicia el formulario
          this.registerForm.reset();
          this.selectedPhoto="";
          this.selectedPhotobase64="/assets/img/camera-icon.png";
          //se presenta la alerta 
        });
        });
      });
  }  
  ///function to convert datauri to BLOB
  dataURItoBlob(dataURI) {
    // codej adapted from:
    //  http://stackoverflow.com/questions/33486352/
    //cant-upload-image-to-aws-s3-from-ionic-camera
        let binary = atob(dataURI.split(',')[1]);
        let array = [];
        for (let i = 0; i < binary.length; i++) {
          array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {type: 'image/jpeg'});
}; 
  
}
