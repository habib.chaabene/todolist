import { AngularFirestore } from '@angular/fire/firestore';
import { Component, OnInit } from '@angular/core';
import { Notification } from '../services/notification';
import { trigger, state, style } from '@angular/animations';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
  //for animation purpose
  animations: [
    trigger('itemState', [
      state('idle', style({
        opacity: '0.3',
        transform: 'scale(1)'
      })),
    ])
  ]
})
export class NotificationPage implements OnInit {
  notifications: Notification[];

  constructor(private afs:AngularFirestore) { }

  ngOnInit() {
    this.afs.collection('notification').snapshotChanges().subscribe(rdvs => {

      this.notifications = rdvs.map(item => {
        console.log(item);
        let uid = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { uid, ...(data as {}) } as Notification;
      });
      console.log(this.notifications);
    });
  }

}
