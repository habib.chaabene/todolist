import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  firstTime: boolean;

  constructor(private storage: Storage) { }

  saveFirstTimeLoad(): void {
    this.storage.set('firstTime', true);
  }

  isFirstTimeLoad(): void {
    this.storage.get("firstTime").then((result) => {
      if (result != null) {
        this.firstTime = false;
      }
      else {
        this.firstTime = true;
      }
    })
  }
}