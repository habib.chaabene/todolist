import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { MbscModule } from '@mobiscroll/angular-lite';
import { CategorieService } from '../services/categorie.service';

@Component({
  selector: 'app-edit-categorie',
  templateUrl: './edit-categorie.page.html',
  styleUrls: ['./edit-categorie.page.scss'],
})
export class EditCategoriePage implements OnInit {

  modalTitle: string;
  modelId: number;
  proForm: FormGroup;
  progression: number;
  user: any;
  categorie: any;
 
  constructor(
    private modalController: ModalController,
    public fb: FormBuilder,
    private router: Router,
    private categorieService:CategorieService
  ) { }
    
  externalSettings: MbscModule  = {
    controls: ['calendar', 'time'],
    showOnTap: false,
    showOnFocus: false
};

  ngOnInit() {
    this.categorie = JSON.parse(localStorage.getItem('categorie'));
    this.user = JSON.parse(localStorage.getItem('userInfo'));
     //this.type = this.user.user.type;
      console.log(this.categorie);
    this.proForm = this.fb.group({
      titre: [this.categorie.titre],
      user_id: [this.user.uid]
    })
    
  }
  formSubmit() {
    if (!this.proForm.valid) {
      return false;
    } else {
      //this.proForm.progression = 0;
      console.log(this.proForm.value);
       this.progression = 0;
      this.categorieService.updateCategorie(this.proForm.value,this.categorie.uid).then(res => {
        //console.log(res);
        this.proForm.reset();
        this.router.navigate(['/categories']);
      },error => console.log(error));
       // .catch(error => console.log(error));
       
    }
  }
 
  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }
  minDate: Date = new Date(2017, 11, 1);
    maxDate: Date = new Date(2017, 11, 10);

    myDateOptions: MbscModule = {
        display: 'top',
        theme: 'ios',
        max: new Date(),
        min: new Date(1960, 0, 1)
    };

    birthday: Date;
    appointment: Array<Date> = [];

}
