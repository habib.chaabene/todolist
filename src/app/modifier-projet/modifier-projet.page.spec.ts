import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierProjetPage } from './modifier-projet.page';

describe('ModifierProjetPage', () => {
  let component: ModifierProjetPage;
  let fixture: ComponentFixture<ModifierProjetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierProjetPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierProjetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
