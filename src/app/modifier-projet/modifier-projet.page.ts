import { DatePipe, registerLocaleData } from '@angular/common';
import { Component, LOCALE_ID, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';
import { MbscModule } from '@mobiscroll/angular-lite';
import { ProjetService } from '../services/projet.service';
import localeFr from '@angular/common/locales/fr';

registerLocaleData(localeFr);

@Component({
  selector: 'app-modifier-projet',
  templateUrl: './modifier-projet.page.html',
  styleUrls: ['./modifier-projet.page.scss'],
  providers: [DatePipe,{ provide: LOCALE_ID, useValue: "fr-FR" }]
})
export class ModifierProjetPage implements OnInit {

  modalTitle: string;
  modelId: number;
  proForm: FormGroup;
  progression: number;
  user: any;
  projet: any;
  today: any;
  min: string;
 
  constructor(
    private modalController: ModalController,
    public fb: FormBuilder,
    private router: Router,
    private datePipe: DatePipe,
    private projetService:ProjetService,
    private toastCtrl:ToastController
  ) { }
    
  externalSettings: MbscModule  = {
    controls: ['calendar', 'time'],
    showOnTap: false,
    showOnFocus: false
};

  ngOnInit() {
    this.projet = JSON.parse(localStorage.getItem('projet'));
    this.today = this.projet.date_debut;
    this.user = JSON.parse(localStorage.getItem('userInfo'));
     //this.type = this.user.user.type;
      console.log(this.projet);
    this.proForm = this.fb.group({
      titre: [this.projet.titre],
      description: [this.projet.description],
      progression: [this.projet.progression],
      date_debut: [this.projet.date_debut],
      date_fin: [this.projet.date_fin],
      user_id: [this.user.uid],
    })
    
  }
  formSubmit() {
    if (!this.proForm.valid) {
      return false;
    } else {
      //this.proForm.progression = 0;
      console.log(this.proForm.value);
       this.progression = 0;
      this.projetService.updateProjet(this.proForm.value,this.projet.id).then(res => {
        //console.log(res);
        this.proForm.reset();
        this.openToast();
        this.router.navigate(['/projets']);
      },error => console.log(error));
       // .catch(error => console.log(error));
       
    }
  }

  async openToast() {   
    const toast = await this.toastCtrl.create({  
      message: 'projet modifier ave succes',  
      animated: false,  
      duration: 8000,
        
      position: 'middle',  
    });  
    toast.present();  
    toast.onDidDismiss().then((val) => {  
      console.log('Toast Dismissed');   
    });  
  } 
 
  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }

  change(ev){
    this.min = this.datePipe.transform(ev.detail.value, 'yyyy-mm-dd');
    console.log('min',this.min);
    console.log(ev.detail.value);
  }

  
  minDate: Date = new Date(2017, 11, 1);
    maxDate: Date = new Date(2017, 11, 10);

    myDateOptions: MbscModule = {
        display: 'top',
        theme: 'ios',
        max: new Date(),
        min: new Date(1960, 0, 1)
    };

    birthday: Date;
    appointment: Array<Date> = [];


}
