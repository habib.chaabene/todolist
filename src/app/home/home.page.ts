import { AngularFirestore } from '@angular/fire/firestore';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { TaskService } from './../services/task.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController, NavController, IonSlides, ToastController, ActionSheetController, AlertController } from '@ionic/angular';
import { ThemeService } from '../services/theme.service';
import { ViewChild } from '@angular/core';
import { CustomThemeService } from '../services/custom-theme.service';
import { ElementRef } from '@angular/core';
import { trigger, state, style } from '@angular/animations';
import { AjouterTachePage } from '../ajouter-tache/ajouter-tache.page';
import { Task } from '../services/task';
import { DatePipe } from '@angular/common';

const themes = {
  //red color o.k
  autumn: {
    primary: '#F44336',
    secondary: '#F44336',
    tertiary: 'white',
    light: '#F44336',
    dark: 'black',
    medium: '#F44336',

  },
  //purple color o.k
  night: {
    primary: '#673AB7',
    secondary: '#673AB7',
    tertiary: '#673AB7',
    medium: '#673AB7',
    dark: 'black',
    light: '#673AB7'
  },
  //blue color o.k
  neon: {
    primary: '#03A9F4',//bold text
    secondary: '#03A9F4',
    tertiary: '#03A9F4',
    light: '#03A9F4',
    dark: 'black',
    medium: '#03A9F4',
  },
  //green color o.k
  orginal: {
    primary: '#4CAF50',
    secondary: '#4CAF50',
    tertiary: '#4CAF50',
    light: '#4CAF50',
    medium: '#4CAF50',
    dark: 'black'
  },
  //gray color o.k
  red: {
    primary: '#9E9E9E',
    secondary: '#9E9E9E',
    tertiary: '#9E9E9E',
    light: '#9E9E9E',//background
    medium: '#9E9E9E',//all btn color
    dark: 'black',//text color
    warning: '#9E9E9E',
  },
  //sharp pink color o.k
  purple: {
    primary: '#E91E63',
    secondary: '#E91E63',
    tertiary: '#E91E63',
    light: '#E91E63',//background
    medium: '#E91E63',//all btn color
    dark: 'black',//text color
    warning: '#E91E63',
  },
  //dark blue color o.k
  Lightblue: {
    primary: '#3F51B5',
    secondary: '#3F51B5',
    tertiary: '#3F51B5',
    light: '#3F51B5',//background
    medium: '#3F51B5',//all btn color
    dark: 'black',//text color
    warning: '#3F51B5',
  },
  //light blue color o.k
  Lightgreen: {
    primary: '#00BCD4',
    secondary: '#00BCD4',
    tertiary: '#00BCD4',
    light: '#00BCD4',//background
    medium: '#00BCD4',//all btn color
    dark: 'black',//text color
    warning: '#00BCD4',
  },
  //light green color o.k
  Lightgray: {
    primary: '#8BC34A',
    secondary: '#8BC34A',
    tertiary: '#8BC34A',
    light: '#8BC34A',//background
    medium: '#8BC34A',//all btn color
    dark: 'black',//text color
    warning: '#8BC34A',
  },
  //dark green color o.k
  blue: {
    primary: '#008577',
    secondary: '#008577',
    tertiary: '#008577',
    light: '#008577',//background
    medium: '#008577',//all btn color
    dark: 'black',//text color
    warning: '#008577',
  }
};
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [DatePipe],
  //for animation purpose
  animations: [
    trigger('itemState', [
      state('idle', style({
        opacity: '0.3',
        transform: 'scale(1)'
      })),
    ])
  ]
})
export class HomePage  implements OnInit {
  clickSub: any;
  visiableBtnAutum = false;
  visiableBtnNight = false;
  visiableBtnNeon = false;
  visiableBtnOriginal = false;
  visiableBtnRed = false;
  visiableBtnPurple = false;
  visiableBtnLightblue = false;
  visiableBtnLightgreen = false;
  visiableBtnLightgray = false;
  visiableBtnBlue = false;
  visiableBtn = false;
  public itemColor = "";
  @ViewChild(IonSlides, {static: false}) autoSlides: IonSlides;
  public items1 = [
    { text: "Baby sleeping bed", img: "assets/images/Pictures/app-slicing/baby_sleeping_bed.png" },
    { text: "Girls dress red floral", img: "assets/images/Pictures/app-slicing/girls_dress_red_floral.png" },
    { text: "Cell Phone Stand", img: "assets/images/Pictures/app-slicing/cell_phone_stand.png" },
    { text: "Red and black chair", img: "assets/images/Pictures/app-slicing/red_and_black_chair.png" },
    { text: "Polo shirt men", img: "assets/images/Pictures/app-slicing/polo_shirt_men_pack.png" },
    { text: "Ladies Jacket", img: "assets/images/Pictures/app-slicing/ladies_jacket_yellow.png" },
    { text: "Baby sleeping bed", img: "assets/images/Pictures/app-slicing/baby_sleeping_bed.png" },
    { text: "Girls dress red floral", img: "assets/images/Pictures/app-slicing/girls_dress_red_floral.png" },
    { text: "Cell Phone Stand", img: "assets/images/Pictures/app-slicing/cell_phone_stand.png" },
    { text: "Red and black chair", img: "assets/images/Pictures/app-slicing/red_and_black_chair.png" },
    { text: "Polo shirt men", img: "assets/images/Pictures/app-slicing/polo_shirt_men_pack.png" },
    { text: "Ladies Jacket", img: "assets/images/Pictures/app-slicing/ladies_jacket_yellow.png" },
    { text: "Baby sleeping bed", img: "assets/images/Pictures/app-slicing/baby_sleeping_bed.png", },
    { text: "Girls dress red floral", img: "assets/images/Pictures/app-slicing/girls_dress_red_floral.png" },
    { text: "Cell Phone Stand", img: "assets/images/Pictures/app-slicing/cell_phone_stand.png" },
    { text: "Red and black chair", img: "assets/images/Pictures/app-slicing/red_and_black_chair.png", },
    { text: "Polo shirt men", img: "assets/images/Pictures/app-slicing/polo_shirt_men_pack.png" },
    { text: "Ladies Jacket", img: "assets/images/Pictures/app-slicing/ladies_jacket_yellow.png" },
    { text: "Baby sleeping bed", img: "assets/images/Pictures/app-slicing/baby_sleeping_bed.png" },
    { text: "Girls dress red floral", img: "assets/images/Pictures/app-slicing/girls_dress_red_floral.png" },
  ];
  public items = [];
  dataReturned: any;
  user: any;
  taches: any[];
  date: Date;
  today: string;
  hour: string;
  constructor(public toastCtrl: ToastController,    
    public alertController: AlertController,
    private localNotifications: LocalNotifications,
    public router:Router,
    private datePipe: DatePipe,
    private taskservice: TaskService,
    private afs: AngularFirestore,
    public modalController: ModalController,private service: CustomThemeService,
    public menuCtrl: MenuController, private theme: ThemeService,
    public actionsheetCtrl: ActionSheetController ,
    private navCtrl: NavController, private elementRef: ElementRef) {
    this.visiableBtnAutum = true;
    this.itemColor = "#F44336";
    this.elementRef.nativeElement.style.setProperty('--my-var', this.itemColor);
    //loop for iteration on items with animation after 0.25 sec
    for (let i = 0; i < 20; i++) {
      setTimeout(() => {
        this.items.push(this.items1[i]);
      }, 250 * i);
    }
    this.user = JSON.parse(localStorage.getItem('user'));
     //this.type = this.user.user.type;
      console.log(this.user);
  }

  async openMenu(task) {  
    const actionSheet = await this.actionsheetCtrl.create({  
      header: 'Modify your album',  
      buttons: [  
        {  
          text: 'Modifier',  
          role: 'destructive',  
          handler: () => {  
            localStorage.setItem('tache', JSON.stringify(task));
    console.log(task);
    this.navCtrl.navigateForward(['modifier-tache']);
          }  
        },{  
          text: 'Supprimer',  
          handler: () => {  
            this.taskservice.deletetask(task.id);
            this.openToast();
          }  
        }, {  
          text: 'Fermer',  
          role: 'cancel',  
          handler: () => {  
            console.log('Cancel clicked');  
          }  
        }  
      ]  
    });  
    await actionSheet.present();  
  }   
  
  async openToast() {   
    const toast = await this.toastCtrl.create({  
      message: 'tache supprimer ave succes',  
      animated: false,  
      duration: 8000,
        
      position: 'middle',  
    });  
    toast.present();  
    toast.onDidDismiss().then((val) => {  
      console.log('Toast Dismissed');   
    });  
  } 
  ngOnInit() {
    setInterval(() => {
      this.getnotif();
    }, 51000);
    this.user = JSON.parse(localStorage.getItem('userInfo'));
     //this.type = this.user.user.type;
      console.log(this.user);
    
    this.taskservice.getTasks(this.user.uid).subscribe(rdvs => {

      this.taches = rdvs.map(item => {
        console.log(item);
        let id = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { id, ...(data as {}) } as Task;
      });
    });
    
  }

  getnotif(){
    this.date= new Date();
    this.today = this.datePipe.transform(this.date, 'yyyy-MM-dd');
    //this.hour = this.datePipe.transform(this.date, 'hh:mm');
    //let heure  = moment(this.hour).add(30, 'm').toDate();

    var d2 = new Date (this.date);
    d2.setMinutes ( this.date.getMinutes() + 30 );
    //console.log( d2);
    this.hour = this.datePipe.transform(d2, 'HH:mm');
    //console.log('today',this.hour);
    
    this.taskservice.getTodayTasks().subscribe(rdvs => {

      this.taches = rdvs.map(item => {
        //console.log(item);
        let id = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { id, ...(data as {}) } as Task;
      });
      console.log('taches',this.taches);
      this.taches.forEach(x=>{
        let heure = this.datePipe.transform(x.date_debut, 'HH:mm');
        console.log('haure',this.hour,heure);
        if(heure == this.hour)
          {
            console.log('haure',this.hour,heure);
            console.log('ok');
            let Notif={};
            Notif['user_id']= this.user.uid;
            let record = Object.assign(Notif, x);
            this.afs.collection('notification').add(record)
            this.multipleNotif(x);
          }
      })
    });
  }

  multipleNotif(x) {
    this.clickSub = this.localNotifications.on('click').subscribe(data => {
      console.log(data);
      // this.presentAlert('Your notifiations contains a secret = ' + data.data.secret);
      this.unsub();
    });
    this.localNotifications.schedule({
      title: x.nom,
      text: x.date_debut,
      actions: [
        { id: 'yes', title: 'Confirmer' },
        { id: 'no', title: 'Annuler' }
      ]
    });
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false, 'Menu2')
    this.menuCtrl.enable(true, 'Menu1')
    this.menuCtrl.open('Menu1');
    this.theme.getTheme().then((result) => {
      let val = result;
      if (val == undefined) {
        this.theme.setTheme(themes['autumn'], 'autumn');
      }
      else {
        this.changeTheme(val);
      }
    });
  }
  ////////////////////////////////
  changeTheme(name) {
    this.theme.setTheme(themes[name], name);
    this.service.setTheme(name);
    if (name == 'autumn') {
      this.itemColor = "#F44336";//red
      this.elementRef.nativeElement.style.setProperty('--my-var', this.itemColor);
      this.visiableBtnAutum = true;
      this.visiableBtnNight = false;
      this.visiableBtnNeon = false;
      this.visiableBtnOriginal = false;
      this.visiableBtnRed = false;
      this.visiableBtnPurple = false;
      this.visiableBtnLightblue = false;
      this.visiableBtnLightgreen = false;
      this.visiableBtnLightgray = false;
      this.visiableBtnBlue = false;
    }
    else if (name == 'night') {
      this.itemColor = "#673AB7";//purple
      this.elementRef.nativeElement.style.setProperty('--my-var', this.itemColor);
      this.visiableBtnAutum = false;
      this.visiableBtnNight = true;
      this.visiableBtnNeon = false;
      this.visiableBtnOriginal = false;
      this.visiableBtnRed = false;
      this.visiableBtnPurple = false;
      this.visiableBtnLightblue = false;
      this.visiableBtnLightgreen = false;
      this.visiableBtnLightgray = false;
      this.visiableBtnBlue = false;
    }
    else if (name == 'neon') {
      this.itemColor = "#03A9F4";//blue
      this.elementRef.nativeElement.style.setProperty('--my-var', this.itemColor);
      this.visiableBtnAutum = false;
      this.visiableBtnNight = false;
      this.visiableBtnNeon = true;
      this.visiableBtnOriginal = false;
      this.visiableBtnRed = false;
      this.visiableBtnPurple = false;
      this.visiableBtnLightblue = false;
      this.visiableBtnLightgreen = false;
      this.visiableBtnLightgray = false;
      this.visiableBtnBlue = false;
    }
    else if (name == 'orginal') {
      this.itemColor = "#4CAF50";//green
      this.elementRef.nativeElement.style.setProperty('--my-var', this.itemColor);
      this.visiableBtnAutum = false;
      this.visiableBtnNight = false;
      this.visiableBtnNeon = false;
      this.visiableBtnOriginal = true;
      this.visiableBtnRed = false;
      this.visiableBtnPurple = false;
      this.visiableBtnLightblue = false;
      this.visiableBtnLightgreen = false;
      this.visiableBtnLightgray = false;
      this.visiableBtnBlue = false;
    }
    else if (name == 'red') {
      this.itemColor = "#9E9E9E";//gray
      this.elementRef.nativeElement.style.setProperty('--my-var', this.itemColor);
      this.visiableBtnAutum = false;
      this.visiableBtnNight = false;
      this.visiableBtnNeon = false;
      this.visiableBtnOriginal = false;
      this.visiableBtnRed = true;
      this.visiableBtnPurple = false;
      this.visiableBtnLightblue = false;
      this.visiableBtnLightgreen = false;
      this.visiableBtnLightgray = false;
      this.visiableBtnBlue = false;
    }
    else if (name == 'purple') {
      this.itemColor = "#E91E63";//sharp pink
      this.elementRef.nativeElement.style.setProperty('--my-var', this.itemColor);
      this.visiableBtnAutum = false;
      this.visiableBtnNight = false;
      this.visiableBtnNeon = false;
      this.visiableBtnOriginal = false;
      this.visiableBtnRed = false;
      this.visiableBtnPurple = true;
      this.visiableBtnLightblue = false;
      this.visiableBtnLightgreen = false;
      this.visiableBtnLightgray = false;
      this.visiableBtnBlue = false;
    }
    else if (name == 'Lightblue') {
      this.itemColor = "#3F51B5";//dark blue
      this.elementRef.nativeElement.style.setProperty('--my-var', this.itemColor);
      this.visiableBtnAutum = false;
      this.visiableBtnNight = false;
      this.visiableBtnNeon = false;
      this.visiableBtnOriginal = false;
      this.visiableBtnRed = false;
      this.visiableBtnPurple = false;
      this.visiableBtnLightblue = true;
      this.visiableBtnLightgreen = false;
      this.visiableBtnLightgray = false;
      this.visiableBtnBlue = false;
    }
    else if (name == 'Lightgreen') {
      this.itemColor = "#00BCD4";//light blue
      this.elementRef.nativeElement.style.setProperty('--my-var', this.itemColor);
      this.visiableBtnAutum = false;
      this.visiableBtnNight = false;
      this.visiableBtnNeon = false;
      this.visiableBtnOriginal = false;
      this.visiableBtnRed = false;
      this.visiableBtnPurple = false;
      this.visiableBtnLightblue = false;
      this.visiableBtnLightgreen = true;
      this.visiableBtnLightgray = false;
      this.visiableBtnBlue = false;
    }
    else if (name == 'Lightgray') {
      this.itemColor = "#8BC34A";//light green
      this.elementRef.nativeElement.style.setProperty('--my-var', this.itemColor);
      this.visiableBtnAutum = false;
      this.visiableBtnNight = false;
      this.visiableBtnNeon = false;
      this.visiableBtnOriginal = false;
      this.visiableBtnRed = false;
      this.visiableBtnPurple = false;
      this.visiableBtnLightblue = false;
      this.visiableBtnLightgreen = false;
      this.visiableBtnLightgray = true;
      this.visiableBtnBlue = false;
    }
    else if (name == 'blue') {
      this.itemColor = "#008577";//dark green
      this.elementRef.nativeElement.style.setProperty('--my-var', this.itemColor);
      this.visiableBtnAutum = false;
      this.visiableBtnNight = false;
      this.visiableBtnNeon = false;
      this.visiableBtnOriginal = false;
      this.visiableBtnRed = false;
      this.visiableBtnPurple = false;
      this.visiableBtnLightblue = false;
      this.visiableBtnLightgreen = false;
      this.visiableBtnLightgray = false;
      this.visiableBtnBlue = true;
    }
  }
  ///////////////////////////////
  public slides = [
    { image: "assets/images/home-banners/banner1.jpg" },
    { image: "assets/images/home-banners/banner2.jpg" },
    { image: "assets/images/home-banners/banner3.jpg" },
  ];
  /////////banner slides autoplay function////////////////
  ionViewDidEnter() {
    
  }
  async openModal() {
    this.navCtrl.navigateForward("ajouter-tache");   
  }
  simpleNotif() {
    this.clickSub = this.localNotifications.on('click').subscribe(data => {
      console.log(data);
      // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
      this.presentAlert('Your notifiations contains a secret = ' + data.data.secret);
      this.unsub();
    });
    this.localNotifications.schedule({
      id: 1,
      text: 'Single Local Notification',
      data: { secret: 'secret' }
    });

  }

  async presentAlert(data) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: data,
      buttons: ['OK']
    });

    await alert.present();
  }
  unsub() {
    this.clickSub.unsubscribe();
  }
}
