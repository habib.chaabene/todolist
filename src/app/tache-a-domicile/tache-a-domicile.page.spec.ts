import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TacheADomicilePage } from './tache-a-domicile.page';

describe('TacheADomicilePage', () => {
  let component: TacheADomicilePage;
  let fixture: ComponentFixture<TacheADomicilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TacheADomicilePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TacheADomicilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
