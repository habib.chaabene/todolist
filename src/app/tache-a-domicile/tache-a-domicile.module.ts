import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TacheADomicilePage } from './tache-a-domicile.page';

const routes: Routes = [
  {
    path: '',
    component: TacheADomicilePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TacheADomicilePage]
})
export class TacheADomicilePageModule {}
