import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-gift',
  templateUrl: './gift.page.html',
  styleUrls: ['./gift.page.scss'],
})
export class GiftPage implements OnInit {
  videourl: any;
  gifturl: any;
  //InAppBrowser: any;
  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
};
  gift: string;

  constructor( private activeRoute:ActivatedRoute,
    public modalCtrl:ModalController,
    public inAppBrowser: InAppBrowser,
    public  sanitizer:DomSanitizer) {
      //console.log(' this.navParms', this.navParms.data.url)
     }

  ngOnInit() {
    this.gift = localStorage.getItem('gift');
    this.gifturl = 'market://details?id='+this.gift;
       console.log(this.gifturl,this.gift);
  }
  dismiss(){
    
    this.modalCtrl.dismiss();
    this.game();
  }
  game(){
    let target = "_system";
    this.inAppBrowser.create(this.gifturl,target,this.options);
    //this.inAppBrowser.create(this.gifturl);
  }
}
