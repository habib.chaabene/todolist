import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GiftPage } from './gift.page';

const routes: Routes = [
  {
    path: '',
    component: GiftPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  
  entryComponents: [GiftPage],
  declarations: [GiftPage],
  exports: [GiftPage]
})
export class GiftPageModule {}
