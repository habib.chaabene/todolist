import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'type-user',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'form-register-three', loadChildren: './form-register-three/form-register-three.module#FormRegisterThreePageModule' },
  { path: 'tab-chat', loadChildren: './tab-chat/tab-chat.module#TabChatPageModule' },
  { path: 'tab-status', loadChildren: './tab-status/tab-status.module#TabStatusPageModule' },
  { path: 'tab-call', loadChildren: './tab-call/tab-call.module#TabCallPageModule' },
  //{ path: 'component-details', loadChildren: './component-details/component-details.module#ComponentDetailsPageModule' },
  { path: 'intro', loadChildren: './intro/intro.module#IntroPageModule' },
  { path: 'form-forget-three', loadChildren: './form-forget-three/form-forget-three.module#FormForgetThreePageModule' },
  { path: 'taches', loadChildren: './taches/taches.module#TachesPageModule' },
  { path: 'projets', loadChildren: './projets/projets.module#ProjetsPageModule' },
  { path: 'parameter', loadChildren: './parameter/parameter.module#ParameterPageModule' },
  { path: 'aujourdhui', loadChildren: './aujourdhui/aujourdhui.module#AujourdhuiPageModule' },
  { path: 'cettesemaine', loadChildren: './cettesemaine/cettesemaine.module#CettesemainePageModule' },
  { path: 'compte', loadChildren: './parameter/compte/compte.module#ComptePageModule' },
  { path: 'ajouter-tache', loadChildren: './ajouter-tache/ajouter-tache.module#AjouterTachePageModule' },
  { path: 'home-enfants', loadChildren: './home-enfants/home-enfants.module#HomeEnfantsPageModule' },
  { path: 'homepersonne', loadChildren: './homepersonne/homepersonne.module#HomepersonnePageModule' },
  { path: 'modifier-tache', loadChildren: './modifier-tache/modifier-tache.module#ModifierTachePageModule' },
  { path: 'modifier-projet', loadChildren: './modifier-projet/modifier-projet.module#ModifierProjetPageModule' },
  { path: 'ajouter-projet', loadChildren: './ajouter-projet/ajouter-projet.module#AjouterProjetPageModule' },
  { path: 'choisir-enfants', loadChildren: './choisir-enfants/choisir-enfants.module#ChoisirEnfantsPageModule' },
  { path: 'image-enfants', loadChildren: './image-enfants/image-enfants.module#ImageEnfantsPageModule' },
  { path: 'accueil-enfants', loadChildren: './accueil-enfants/accueil-enfants.module#AccueilEnfantsPageModule' },
  { path: 'home-enfants', loadChildren: './home-enfants/home-enfants.module#HomeEnfantsPageModule' },
  { path: 'listes-utilisateurs', loadChildren: './listes-utilisateurs/listes-utilisateurs.module#ListesUtilisateursPageModule' },
  { path: 'options-enfants', loadChildren: './options-enfants/options-enfants.module#OptionsEnfantsPageModule' },
  { path: 'home-etudiant', loadChildren: './home-etudiant/home-etudiant.module#HomeEtudiantPageModule' },
  { path: 'taches-realises', loadChildren: './taches-realises/taches-realises.module#TachesRealisesPageModule' },
  { path: 'tache-personnalises', loadChildren: './tache-personnalises/tache-personnalises.module#TachePersonnalisesPageModule' },
  { path: 'tache-a-domicile', loadChildren: './tache-a-domicile/tache-a-domicile.module#TacheADomicilePageModule' },
  { path: 'tache-culturel', loadChildren: './tache-culturel/tache-culturel.module#TacheCulturelPageModule' },
  { path: 'autres-taches', loadChildren: './autres-taches/autres-taches.module#AutresTachesPageModule' },
  { path: 'view-video', loadChildren: './view-video/view-video.module#ViewVideoPageModule' },
  { path: 'addtaskenfants', loadChildren: './addtaskenfants/addtaskenfants.module#AddtaskenfantsPageModule' },
  { path: 'type-user', loadChildren: './type-user/type-user.module#TypeUserPageModule' },
  { path: 'intro-enfants', loadChildren: './intro-enfants/intro-enfants.module#IntroEnfantsPageModule' },
  { path: 'step-one', loadChildren: './step-one/step-one.module#StepOnePageModule' },
  { path: 'calendar', loadChildren: './calendar/calendar.module#CalendarPageModule' },
  { path: 'day-tache', loadChildren: './day-tache/day-tache.module#DayTachePageModule' },
  { path: 'theme', loadChildren: './parameter/theme/theme.module#ThemePageModule' },
  { path: 'encour', loadChildren: './encour/encour.module#EncourPageModule' },
  { path: 'terminer', loadChildren: './terminer/terminer.module#TerminerPageModule' },
  { path: 'calandar-enffant', loadChildren: './calandar-enffant/calandar-enffant.module#CalandarEnffantPageModule' },
  { path: 'login-enfant', loadChildren: './login-enfant/login-enfant.module#LoginEnfantPageModule' },
  { path: 'tache-enfant', loadChildren: './tache-enfant/tache-enfant.module#TacheEnfantPageModule' },
  { path: 'gift', loadChildren: './gift/gift.module#GiftPageModule' },
  { path: 'categories', loadChildren: './categories/categories.module#CategoriesPageModule' },
  { path: 'add-categorie', loadChildren: './add-categorie/add-categorie.module#AddCategoriePageModule' },
  { path: 'edit-categorie', loadChildren: './edit-categorie/edit-categorie.module#EditCategoriePageModule' },
  { path: 'modifier-compte', loadChildren: './modifier-compte/modifier-compte.module#ModifierComptePageModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'modifier-profile', loadChildren: './modifier-profile/modifier-profile.module#ModifierProfilePageModule' },
  { path: 'modifier-tacheenfant', loadChildren: './modifier-tacheenfant/modifier-tacheenfant.module#ModifierTacheenfantPageModule' },
  {
    path: 'notification',
    loadChildren: () => import('./notification/notification.module').then( m => m.NotificationPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
