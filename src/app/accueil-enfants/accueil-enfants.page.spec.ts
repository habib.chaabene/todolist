import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccueilEnfantsPage } from './accueil-enfants.page';

describe('AccueilEnfantsPage', () => {
  let component: AccueilEnfantsPage;
  let fixture: ComponentFixture<AccueilEnfantsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccueilEnfantsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccueilEnfantsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
