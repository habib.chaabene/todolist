import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AccueilEnfantsPage } from './accueil-enfants.page';
//all paths for tabs navigation are define here below
const routes: Routes = [
  {
    path: 'accueil-enfants',
    component: AccueilEnfantsPage,
    children: [
      {
        path: 'tab-chat',
        loadChildren: '../tab-chat/tab-chat.module#TabChatPageModule'
      },
      {
        path: 'calendar-enffant',
        loadChildren: '../calandar-enffant/calandar-enffant.module#CalandarEnffantPageModule'
      },
      {
        path: 'addtaskenfants',
        loadChildren: '../addtaskenfants/addtaskenfants.module#AddtaskenfantsPageModule'
      },
      {
        path: 'modifier-tacheenfant',
        loadChildren: '../modifier-tacheenfant/modifier-tacheenfant.module#ModifierTacheenfantPageModule'
      },
      {
        path: 'tab-call',
        loadChildren: '../tab-call/tab-call.module#TabCallPageModule'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'accueil-enfants/tab-chat',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule, 
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AccueilEnfantsPage]
})
export class AccueilEnfantsPageModule { }
