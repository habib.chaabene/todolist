import { Subscription } from 'rxjs';
import { Component, ElementRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, MenuController, NavController, Platform } from '@ionic/angular';
import { CustomThemeService } from '../services/custom-theme.service';
import { ThemeService } from '../services/theme.service';


@Component({
  selector: 'app-accueil-enfants',
  templateUrl: './accueil-enfants.page.html',
  styleUrls: ['./accueil-enfants.page.scss'],
})
export class AccueilEnfantsPage implements OnInit {
  
  dataReturned: any;
  
  private backButtonSub: Subscription;
  
  constructor(
    
    private platform: Platform,
    public router:Router,
    public modalController: ModalController,private service: CustomThemeService,
     public menuCtrl: MenuController, private theme: ThemeService,
    private navCtrl: NavController, private elementRef: ElementRef) {
    
  }

  ngOnInit() {
    this.menuCtrl.enable(false, 'custom')
    this.menuCtrl.enable(false, 'custom')
    
  }
  
  ionViewDidEnter() {
    this.backButtonSub = this.platform.backButton.subscribeWithPriority(
      10000,
      () => this.onBack()
    );
  }

  ionViewWillLeave() {
    this.backButtonSub.unsubscribe();
  }

  private async onBack() {
    const openMenu = await this.menuCtrl.getOpen();
    if (openMenu) {
      await openMenu.close();
    } else {
      await this.navCtrl.pop();
    }
  }
}
