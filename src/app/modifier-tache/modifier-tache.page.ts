import { DatePipe } from '@angular/common';
import { AngularFirestore } from '@angular/fire/firestore';
import { Component, LOCALE_ID, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ModalController, ToastController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { mobiscroll, MbscModule  } from '@mobiscroll/angular-lite';
import { TaskService } from '../services/task.service';
//import { MbscDatetimeOptions } from '../lib/mobiscroll/js/mobiscroll.angular.min.js';
mobiscroll.settings = {
    theme: 'ios',
    themeVariant: 'light',
    display: 'bubble'
};
const now = new Date();
@Component({
  selector: 'app-modifier-tache',
  templateUrl: './modifier-tache.page.html',
  styleUrls: ['./modifier-tache.page.scss'],
  providers: [DatePipe,{ provide: LOCALE_ID, useValue: "fr-FR" }]
})
export class ModifierTachePage implements OnInit {

  modalTitle: string;
  modelId: number;
  external = now;
  tacheForm: FormGroup;
  progression: number;
  task:any;
  tache: any;
  min: any;
  today: any;
  
 
  constructor(private route: ActivatedRoute,
    private modalController: ModalController,
    public fb: FormBuilder,
    private router: Router,
    private datePipe: DatePipe,
    private toastCtrl: ToastController,
    private taskService:TaskService,
    private afs: AngularFirestore
  ) { 
    
  }
    
  externalSettings: MbscModule  = {
    controls: ['calendar', 'time'],
    showOnTap: false,
    showOnFocus: false
};

  ngOnInit() {
    this.tache = JSON.parse(localStorage.getItem('tache'));
    console.log(this.tache);
    this.today = this.tache.date_debut;
    this.tacheForm = this.fb.group({
      nom: [this.tache.nom],
      commentaire: [this.tache.commentaire],
      categorie: [this.tache.categorie],
      priorite: [this.tache.priorite],
      date_debut: [this.tache.date_debut],
      date_fin: [this.tache.date_fin],
      rappel: [this.tache.rappel],
      repetition: [this.tache.repetition],
      progression:[this.tache.progression],
      id:[this.tache.id],
      user_id:[this.tache.user_id]
    })
    
  }
  async openToast() {   
    const toast = await this.toastCtrl.create({  
      message: 'tache modifier ave succes',  
      animated: false,  
      duration: 8000,
        
      position: 'middle',  
    });  
    toast.present();  
    toast.onDidDismiss().then((val) => {  
      console.log('Toast Dismissed');   
    });  
  } 
  formSubmit() {
    if (!this.tacheForm.valid) {
      return false;
    } else {
      //this.tacheForm.progression = 0;
      console.log(this.tacheForm.value);
       this.progression = 0;
      this.taskService.updateTask(this.tacheForm.value).then(res => {
        console.log(res);
        this.tacheForm.reset();
        this.openToast();
        this.router.navigate(['/home']);
      },error => console.log(error));
       // .catch(error => console.log(error));
       
    }
  }
  valider(){

    let record ={};
    record['etat'] = "terminer";
    record['progression'] = 0;
    this.afs.collection('taches').doc(this.tache.uid).update(record)
  }
 
  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }
    onDelete(){
      this.taskService.deletetask(this.tache.id);
      this.router.navigate(['/taches']);
    }

    change(ev){
      this.min = this.datePipe.transform(ev.detail.value, 'yyyy-MM-dd');
      console.log('min',this.min);
      console.log(ev.detail.value);
    }

}
