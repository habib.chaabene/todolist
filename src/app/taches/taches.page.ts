import { Component, OnInit } from '@angular/core';
import { trigger, state, style } from '@angular/animations';
import { TaskService } from '../services/task.service';
import { Task } from '../services/task';
import { NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-taches',
  templateUrl: './taches.page.html',
  styleUrls: ['./taches.page.scss'],
  //for animation purpose
  animations: [
    trigger('itemState', [
      state('idle', style({
        opacity: '0.3',
        transform: 'scale(1)'
      })),
    ])
  ]
})
export class TachesPage implements OnInit {

  user: any;
  taches: any[];

  constructor(private taskservice: TaskService,
    private navCtrl: NavController) {
    this.user = JSON.parse(localStorage.getItem('user'));
     //this.type = this.user.user.type;
      console.log(this.user);
   }

  ngOnInit() {
    
    
    
  }
  async modifier(task) {
    
    localStorage.setItem('tache', JSON.stringify(task));
    console.log(task);
    this.navCtrl.navigateForward(['modifier-tache']);
    /*
    const modal = await this.modalController.create({
      component: AjouterTachePage,
      componentProps: {
        "paramID": 123,
        "paramTitle": "Test Title"
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        //alert('Modal Sent Data :'+ dataReturned);
      }
    });
 
    return await modal.present();
    */
  }
  async openModal() {
    this.navCtrl.navigateForward("ajouter-tache");
    /*
    const modal = await this.modalController.create({
      component: AjouterTachePage,
      componentProps: {
        "paramID": 123,
        "paramTitle": "Test Title"
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        //alert('Modal Sent Data :'+ dataReturned);
      }
    });
 
    return await modal.present();
    */
  }
}
