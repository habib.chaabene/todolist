import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TachesPage } from './taches.page';

const routes: Routes = [
  {
    path: 'taches',
    component: TachesPage,
    children: [
      {
        path: 'encour',
        loadChildren: '../encour/encour.module#EncourPageModule'
      },
      {
        path: 'terminer',
        loadChildren: '../terminer/terminer.module#TerminerPageModule'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'taches/encour',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TachesPage]
})
export class TachesPageModule {}
