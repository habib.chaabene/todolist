import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TachesPage } from './taches.page';

describe('TachesPage', () => {
  let component: TachesPage;
  let fixture: ComponentFixture<TachesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TachesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TachesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
