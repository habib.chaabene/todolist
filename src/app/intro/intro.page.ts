import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {
  @ViewChild(IonSlides, {static: false}) autoSlides: IonSlides;
  indexGlobal: any;
  visiable = false;

  public slides = [
    { image: "assets/images/intro/1.png", title: "Home Page", icon: "home", text: "Home screen contain all themes at top. You can select component screen, UI screens and many more from home page. " },
    { image: "assets/images/intro/2.png", title: "Component Details Page", icon: "apps", text: "Cmponent details page contain all 90+ screens of ionic components. You can use them instead of creating them from scratch" },
    { image: "assets/images/intro/3.jpg", title: "UI screens", icon: "browsers", text: "Comming Soon" },
  ];
  firstTime: boolean;
  constructor(
    private storageService: StorageService,
    private router: Router,
    public menuCtrl: MenuController,
    private splashScreen: SplashScreen) {
  }
  nextSlide() {
    this.autoSlides.slideNext();
  }
  ionViewDidEnter() {
    setTimeout(() => {
      this.splashScreen.hide();
    }, 1500);
    this.autoSlides.startAutoplay();
    this.menuCtrl.enable(false);
  }
  ngOnInit() {

    this.firstTime = this.storageService.firstTime;  

    //if first time update first time 
    if(this.firstTime){
      this.storageService.saveFirstTimeLoad();
    }
    if(!this.firstTime){
      this.router.navigate(['/login']);
    }
    
    let data = localStorage.getItem('FisrtTime');  
    console.log(data);
            if(data == '1')
            {
              //this.router.navigateByUrl('/login');
              this.router.navigate(['/login']);
            }
            if(data == '0')
              {
                localStorage.setItem('FisrtTime', '1')
                this.router.navigate(['/intro']);
              }
  }
  slideChanged() {
    this.autoSlides.getActiveIndex().then(index => {
      console.log(index);
      if (index == 2) {
        this.visiable = true;
      }
      else {
        this.visiable = false;
      }
    });
  }

}
