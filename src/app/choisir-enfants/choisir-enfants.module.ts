import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ChoisirEnfantsPage } from './choisir-enfants.page';

const routes: Routes = [
  {
    path: '',
    component: ChoisirEnfantsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ChoisirEnfantsPage]
})
export class ChoisirEnfantsPageModule {}
