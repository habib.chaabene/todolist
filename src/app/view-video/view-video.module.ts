import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ViewVideoPage } from './view-video.page';

const routes: Routes = [
  {
    path: '',
    component: ViewVideoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [ViewVideoPage],
  declarations: [ViewVideoPage],
  exports: [ViewVideoPage]
})
export class ViewVideoPageModule {}
