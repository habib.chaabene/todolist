import { GiftPage } from './../gift/gift.page';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NavParams, ModalController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-view-video',
  templateUrl: './view-video.page.html',
  styleUrls: ['./view-video.page.scss'],
})
export class ViewVideoPage implements OnInit {
  public videourl;
  counter=60;
  timeLeft: number = 60;
     interval;
  gifturl: any;
  constructor( private activeRoute:ActivatedRoute,
    public alertController: AlertController,
    public modalCtrl:ModalController,
    public navParms:NavParams,
    private router:Router,
    public  sanitizer:DomSanitizer) {
      console.log(' this.navParms', this.navParms.data.url)
        this.videourl = this.navParms.data.url
        this.gifturl = this.navParms.data.gift
       console.log(this.gifturl);

       
     }
        

  ngOnInit() {
  }
  showAlert() {

    this.alertController.create({
      header: 'Alert',
      subHeader: 'أحسنت',
      message: 'يمكنك تحميل اللعبة ',
      buttons: [
        {
          text: 'ok',
          handler: () => {
            localStorage.setItem('gift',this.gifturl);
            this.router.navigate(['/gift']);
            console.log(this.gifturl);
          }
        },
      ]
    }).then(res => {
      res.present();
      //this.viewVideo(this.gifturl);
    });

  }
  dismiss(){
    
    this.modalCtrl.dismiss();
    this.showAlert();
    
  }

  async viewVideo(gift) {
    const modal = await this.modalCtrl.create({
      component: GiftPage,
      componentProps: { url: gift },
      cssClass: 'viewVideoModal'
    })
    return modal.present();
  }

}
