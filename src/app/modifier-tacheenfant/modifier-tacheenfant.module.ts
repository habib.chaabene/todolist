import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ModifierTacheenfantPage } from './modifier-tacheenfant.page';

const routes: Routes = [
  {
    path: '',
    component: ModifierTacheenfantPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ModifierTacheenfantPage]
})
export class ModifierTacheenfantPageModule {}
