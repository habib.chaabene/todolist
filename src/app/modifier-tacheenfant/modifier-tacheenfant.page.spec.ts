import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierTacheenfantPage } from './modifier-tacheenfant.page';

describe('ModifierTacheenfantPage', () => {
  let component: ModifierTacheenfantPage;
  let fixture: ComponentFixture<ModifierTacheenfantPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierTacheenfantPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierTacheenfantPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
