import { TacheEnfantService } from './../services/tache-enfant.service';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormBuilder, FormGroup, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { TaskService } from '../services/task.service';
import { MbscModule } from '@mobiscroll/angular-lite';

@Component({
  selector: 'app-modifier-tacheenfant',
  templateUrl: './modifier-tacheenfant.page.html',
  styleUrls: ['./modifier-tacheenfant.page.scss'],
})
export class ModifierTacheenfantPage implements OnInit {

  progression: number;
  modalTitle: string;
  modelId: number;
  external = new Date();
  tacheForm: FormGroup;
  enfant: any;
  enfant_id: any;
  items: any;
  options: any;
  tache: any;
  

  constructor(
    private modalController: ModalController,
    public fb: FormBuilder,
    private router: Router,
    private taskService:TacheEnfantService
  ) { 
    
  }
  externalSettings: MbscModule  = {
    controls: ['calendar', 'time'],
    showOnTap: false,
    showOnFocus: false
  }

  ngOnInit() {
    this.tache = JSON.parse(localStorage.getItem('tacheenfant'));
    this.enfant = JSON.parse(localStorage.getItem('enfant'));
    this.enfant_id = localStorage.getItem('enfant_id');
    console.log(this.enfant_id);
    this.tacheForm = this.fb.group({
      nom: [this.tache.nom],
      categorie: [this.tache.categorie],
      date_debut:[this.tache.date_debut],
      date_fin: [this.tache.date_fin],
      rappel: [this.tache.rappel],
      user_id:[this.enfant_id],
      id:[this.tache.id]
  })
  }
  onChange(evt) {
    console.log(evt);
  }

formSubmit() {
  if (!this.tacheForm.valid) {
    return false;
  } else {
    //this.tacheForm.progression = 0;
    console.log(this.tacheForm.value);
     this.progression = 0;
    this.taskService.updatetacheenfant(this.tacheForm.value).then(res => {
      console.log(this.tacheForm.value);
      this.tacheForm.reset();
      this.router.navigate(['/tache-enfant']);
    },error => console.log(error));
     // .catch(error => console.log(error));
     
  }
}

async closeModal() {
  this.router.navigate(['/tache-enfant']);
}
minDate: Date = new Date(2017, 11, 1);
  maxDate: Date = new Date(2017, 11, 10);

  myDateOptions: MbscModule = {
      display: 'top',
      theme: 'ios',
      max: new Date(),
      min: new Date(1960, 0, 1)
  };

  birthday: Date;
  appointment: Array<Date> = [];

}
