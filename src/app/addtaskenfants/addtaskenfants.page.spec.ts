import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddtaskenfantsPage } from './addtaskenfants.page';
import { ReactiveFormsModule } from '@angular/forms';

describe('AddtaskenfantsPage', () => {
  let component: AddtaskenfantsPage;
  let fixture: ComponentFixture<AddtaskenfantsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddtaskenfantsPage ],
      imports: [ReactiveFormsModule],  
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddtaskenfantsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
