import { Component, LOCALE_ID, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormBuilder, FormGroup, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { TaskService } from '../services/task.service';
import { MbscModule } from '@mobiscroll/angular-lite';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-addtaskenfants',
  templateUrl: './addtaskenfants.page.html',
  styleUrls: ['./addtaskenfants.page.scss'],
  providers: [DatePipe,{ provide: LOCALE_ID, useValue: "fr-FR" }]
})
export class AddtaskenfantsPage implements OnInit {
  progression: number;
  modalTitle: string;
  modelId: number;
  external = new Date();
  tacheForm: FormGroup;
  enfant: any;
  enfant_id: any;
  items: any;
  options: any;
  date: Date;
  today: string;
  min: string;
  

  constructor(
    private modalController: ModalController,
    public fb: FormBuilder,
    private router: Router,
    private datePipe: DatePipe,
    private taskService:TaskService
  ) { 
    
  }
  externalSettings: MbscModule  = {
    controls: ['calendar', 'time'],
    showOnTap: false,
    showOnFocus: false
  }

  ngOnInit() {
    this.date = new Date();
    let maxDate: any = new Date(new Date().setFullYear(new Date().getFullYear() + 2)).toISOString();
    this.today = this.datePipe.transform(this.date, 'yyyy-MM-dd');
    this.enfant = JSON.parse(localStorage.getItem('enfant'));
    this.enfant_id = localStorage.getItem('enfant_id');
    console.log(this.enfant_id);
    this.tacheForm = this.fb.group({
      nom: [''],
      categorie: [''],
      date_debut: [''],
      date_fin: [''],
      rappel: [''],
      user_id:[this.enfant_id]
  })
  }
  onChange(evt) {
    console.log(evt);
  }

  change(ev){
    this.min = this.datePipe.transform(ev.detail.value, 'yyyy-MM-dd');
    console.log(this.min);
    console.log(ev.detail.value);
  }

formSubmit() {
  if (!this.tacheForm.valid) {
    return false;
  } else {
    //this.tacheForm.progression = 0;
    console.log(this.tacheForm.value);
     this.progression = 0;
    this.taskService.createTaskenfant(this.tacheForm.value).then(res => {
      console.log(this.tacheForm.value);
      this.tacheForm.reset();
      this.router.navigate(['/accueil-enfants/accueil-enfants/tab-call']);
    },error => console.log(error));
     // .catch(error => console.log(error));
     
  }
}

async closeModal() {
  this.router.navigate(['/accueil-enfants/accueil-enfants/tab-chat']);
}
minDate: Date = new Date(2017, 11, 1);
  maxDate: Date = new Date(2017, 11, 10);

  myDateOptions: MbscModule = {
      display: 'top',
      theme: 'ios',
      max: new Date(),
      min: new Date(1960, 0, 1)
  };

  birthday: Date;
  appointment: Array<Date> = [];

}
