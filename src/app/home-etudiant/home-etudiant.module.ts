import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomeEtudiantPage } from './home-etudiant.page';

const routes: Routes = [
  {
    path: '',
    component: HomeEtudiantPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomeEtudiantPage]
})
export class HomeEtudiantPageModule {}
