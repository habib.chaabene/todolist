import { Component, OnInit } from '@angular/core';
import { MbscModule, mobiscroll } from '@mobiscroll/angular-lite';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { CategorieService } from '../services/categorie.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

mobiscroll.settings = {
  theme: 'ios',
  themeVariant: 'light',
  display: 'bubble'
};
const now = new Date();

@Component({
  selector: 'app-add-categorie',
  templateUrl: './add-categorie.page.html',
  styleUrls: ['./add-categorie.page.scss'],
})
export class AddCategoriePage implements OnInit {

  modalTitle: string;
  modelId: number;
  external = now;
  proForm: FormGroup;
  progression: number;
  user: any;
 
  constructor(
    private modalController: ModalController,
    public fb: FormBuilder,
    private socialSharing: SocialSharing,
    private router: Router,
    private categorieService:CategorieService
  ) { }
    
  externalSettings: MbscModule  = {
    controls: ['calendar', 'time'],
    showOnTap: false,
    showOnFocus: false
};

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('userInfo'));
     //this.type = this.user.user.type;
      console.log(this.user);
    this.proForm = this.fb.group({
      titre: [''],
      user_id: [this.user.uid],
    })
    
  }
  formSubmit() {
    if (!this.proForm.valid) {
      return false;
    } else {
      //this.proForm.progression = 0;
      console.log(this.proForm.value);
       this.progression = 0;
      this.categorieService.createCategorie(this.proForm.value).then(res => {
        console.log(res);
        this.proForm.reset();
        this.router.navigate(['/categories']);
      },error => console.log(error));
       // .catch(error => console.log(error));
       
    }
  }

  shareviaWhatsapp(){
    var options = {
      message: "Check this out!",
      subject: "this.product.name",
      files: ['', ''],
      url: "https://git-scm.com/",
      chooserTitle: 'Choose an App'
  }
  
  this.socialSharing.shareWithOptions(options);
}
 
  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }
  minDate: Date = new Date(2017, 11, 1);
    maxDate: Date = new Date(2017, 11, 10);

    myDateOptions: MbscModule = {
        display: 'top',
        theme: 'ios',
        max: new Date(),
        min: new Date(1960, 0, 1)
    };

    birthday: Date;
    appointment: Array<Date> = [];

}
