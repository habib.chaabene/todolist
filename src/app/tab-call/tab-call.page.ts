import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
    selector: 'app-tab-call',
    templateUrl: './tab-call.page.html',
    styleUrls: ['./tab-call.page.scss'],
})
export class TabCallPage implements OnInit {
    user: any;

    constructor(private navCtrl: NavController,) { }
    public items = [
        { text: "مهمات جاهزة",value:'مهمات جاهزة', short_des: "July 16, 1:45 PM", img: "assets/images/Pictures/faces-images/face_image1.png" },
        { text: "مهمات شخصية",value:'مهمات شخصية', short_des: "July 12, 3:45 PM ", img: "assets/images/Pictures/faces-images/face_image2.png" },
        { text: "مهمات منزلية",value:'مهمات منزلية', short_des: "July 10, 1:45 AM", img: "assets/images/Pictures/faces-images/face_image3.png" },
        { text: "مهمات تثقيفية",value:'مهمات تثقيفية', short_des: "July 8, 2:45 PM ", img: "assets/images/Pictures/faces-images/face_image4.png" },
        { text: "مهمات أخرى",value:'مهمات أخرى', short_des: " July 7, 9:45 PM ", img: "assets/images/Pictures/faces-images/face_image5.png" },        
    ];
    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('userInfo'));
        console.log(this.user);
    }

    cat(item){
        localStorage.setItem('categorie',item);
        this.navCtrl.navigateForward('/tache-enfant');
    }
}