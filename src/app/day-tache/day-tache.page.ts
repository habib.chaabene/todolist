import { Component, OnInit } from '@angular/core';
import { trigger, state, style } from '@angular/animations';
import { TaskService } from '../services/task.service';
import { Task } from '../services/task';
import { NavController, ActionSheetController, ToastController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-day-tache',
  templateUrl: './day-tache.page.html',
  styleUrls: ['./day-tache.page.scss'],
  providers: [DatePipe],
  //for animation purpose
  animations: [
    trigger('itemState', [
      state('idle', style({
        opacity: '0.3',
        transform: 'scale(1)'
      })),
    ])
  ]
})
export class DayTachePage implements OnInit {

  user: any;
  taches: any[];
  date= new Date();
  today: any;

  constructor(private taskservice: TaskService,
    public actionsheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    private datePipe: DatePipe,
    private navCtrl: NavController) {
    
   }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('userInfo'));
     //this.type = this.user.user.type;
      console.log(this.user);
    
    this.today = localStorage.getItem('days');
    console.log('today',this.today);
    this.taskservice.getTasks(this.user.uid).subscribe(rdvs => {

      this.taches = rdvs.map(item => {
        //console.log(item);
        let id = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { id, ...(data as {}) } as Task;
      });
    });
    
  }
  

  async openMenu(task) {  
    const actionSheet = await this.actionsheetCtrl.create({  
      header: 'Modify your album',  
      buttons: [  
        {  
          text: 'Modifier',  
          role: 'destructive',  
          handler: () => {  
            localStorage.setItem('tache', JSON.stringify(task));
    console.log(task);
    this.navCtrl.navigateForward(['modifier-tache']);
          }  
        },{  
          text: 'Supprimer',  
          handler: () => {  
            this.taskservice.deletetask(task.id);
            this.openToast();
          }  
        }, {  
          text: 'Fermer',  
          role: 'cancel',  
          handler: () => {  
            console.log('Cancel clicked');  
          }  
        }  
      ]  
    });  
    await actionSheet.present();  
  }   
  
  async openToast() {   
    const toast = await this.toastCtrl.create({  
      message: 'tache supprimer ave succes',  
      animated: false,  
      duration: 8000,
        
      position: 'middle',  
    });  
    toast.present();  
    toast.onDidDismiss().then((val) => {  
      console.log('Toast Dismissed');   
    });  
  } 

  goback(){
    this.navCtrl.back();
  }
  async openModal() {
    this.navCtrl.navigateForward("ajouter-tache");   
  }
 
}
