import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DayTachePage } from './day-tache.page';

describe('DayTachePage', () => {
  let component: DayTachePage;
  let fixture: ComponentFixture<DayTachePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DayTachePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DayTachePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
