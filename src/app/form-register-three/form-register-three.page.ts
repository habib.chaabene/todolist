import { User } from './../services/user';
import { AngularFirestore } from '@angular/fire/firestore';
import { Component, OnInit } from '@angular/core';
import { CustomThemeService } from '../services/custom-theme.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MenuController, NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { ConfirmPasswordValidator } from './confirm-password.validator';

@Component({
  selector: 'app-form-register-three',
  templateUrl: './form-register-three.page.html',
  styleUrls: ['./form-register-three.page.scss'],
})
export class FormRegisterThreePage implements OnInit {
  registerForm: FormGroup;
  errorMessage: string = '';
  users: User[];
  verif: boolean;
  nbr: number;
 
  constructor(
    private menuCtrl: MenuController,
    private navCtrl: NavController,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private afs: AngularFirestore
 
  ) { }
 
  ngOnInit() {
    //this.menuCtrl.enable(false); // or true
 
    this.registerForm = this.formBuilder.group({
      nom:new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[a-zA-Z]+$')
      ])),
      prenom:new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z]+$')
      ])),
      confirmPassword: new FormControl('', Validators.compose([
        Validators.required
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
    },
    {
      validator: ConfirmPasswordValidator("password", "confirmPassword")
    });
  }
  get f() { return this.registerForm.controls; }
  
  checkEmail(email){
    console.log(email.value);
    this.afs.collection('users', ref => ref.where('email', '==', email.value)).snapshotChanges().subscribe(rdvs => {

      this.users = rdvs.map(item => {
       // console.log(item);
        let uid = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { uid, ...(data as {}) } as User;
      });
      this.nbr = this.users.length;
      if(this.users.length == 0){
        this.verif= true;
      }
      if(this.users.length == 1){
        this.verif= false;
      }
      console.log(this.users.length );
    });
  }

  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };
 
 
  registerUser(){
    console.log(this.registerForm.value);
    this.authService.register(this.registerForm.value);
  }
 
  back(){
    this.navCtrl.navigateForward('/login');
  }
}
