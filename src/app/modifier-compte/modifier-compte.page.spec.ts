import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierComptePage } from './modifier-compte.page';

describe('ModifierComptePage', () => {
  let component: ModifierComptePage;
  let fixture: ComponentFixture<ModifierComptePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierComptePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierComptePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
