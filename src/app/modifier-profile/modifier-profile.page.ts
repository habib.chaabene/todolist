import { User } from './../services/user';
import { AngularFirestore } from '@angular/fire/firestore';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';
import { MbscModule } from '@mobiscroll/angular-lite';
import { ProjetService } from '../services/projet.service';
import { map } from 'rxjs/operators'
import { UsersService } from '../services/user.service';


@Component({
  selector: 'app-modifier-profile',
  templateUrl: './modifier-profile.page.html',
  styleUrls: ['./modifier-profile.page.scss'],
})
export class ModifierProfilePage implements OnInit {

  modalTitle: string;
  modelId: number;
  proForm: FormGroup;
  progression: number;
  user: any;
  projet: any;
  users: any;
 
  constructor(
    private modalController: ModalController,
    public fb: FormBuilder,
    private service: UsersService,
    private router: Router,
    private projetService:ProjetService,
    private toastCtrl:ToastController,
    private afs: AngularFirestore
  ) { }
    
  externalSettings: MbscModule  = {
    controls: ['calendar', 'time'],
    showOnTap: false,
    showOnFocus: false
};

  ngOnInit() {
    this.projet = JSON.parse(localStorage.getItem('projet'));
    this.user = JSON.parse(localStorage.getItem('userInfo'));
     //this.type = this.user.user.type;
      console.log(this.projet);
    this.proForm = this.fb.group({
      nom: [this.user.nom],
      prenom: [this.user.prenom],
      email: [this.user.email],
      password: [this.user.password],
      uid:[this.user.uid]
    })
    
  }
  formSubmit() {
    if (!this.proForm.valid) {
      return false;
    } else {
      //this.proForm.progression = 0;
      console.log(this.proForm.value);
       this.progression = 0;

       localStorage.setItem('userInfo', JSON.stringify(this.proForm.value));
      this.afs.collection('users-enfants').doc(this.user.uid).update(this.proForm.value).then(res => {
        console.log(res);
        this.router.navigate(['/profile']);
        
      },error => console.log(error));

      
    }
  }

  async openToast() {   
    const toast = await this.toastCtrl.create({  
      message: 'تم حفظ البيانات بنجاح',  
      animated: false,  
      duration: 8000,
        
      position: 'middle',  
    });  
    toast.present();  
    toast.onDidDismiss().then((val) => {  
      console.log('Toast Dismissed');   
    });  
  } 
 
  async closeModal() {
    this.router.navigate(['/profile']);
  }
  minDate: Date = new Date(2017, 11, 1);
    maxDate: Date = new Date(2017, 11, 10);

    myDateOptions: MbscModule = {
        display: 'top',
        theme: 'ios',
        max: new Date(),
        min: new Date(1960, 0, 1)
    };

    birthday: Date;
    appointment: Array<Date> = [];

}
