import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TachesRealisesPage } from './taches-realises.page';

describe('TachesRealisesPage', () => {
  let component: TachesRealisesPage;
  let fixture: ComponentFixture<TachesRealisesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TachesRealisesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TachesRealisesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
