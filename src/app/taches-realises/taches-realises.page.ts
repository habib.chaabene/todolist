import { Component, OnInit } from '@angular/core';
import { VideoPlayer } from '@ionic-native/video-player/ngx';
import { ModalController } from '@ionic/angular';
import {DomSanitizer} from '@angular/platform-browser';
import { ViewVideoPage } from '../view-video/view-video.page';
@Component({
  selector: 'app-taches-realises',
  templateUrl: './taches-realises.page.html',
  styleUrls: ['./taches-realises.page.scss'],
})
export class TachesRealisesPage implements OnInit {
  videourl: string;

  constructor(private videoPlayer: VideoPlayer, public modalCtrl: ModalController) {
  }
  async viewVideo(urls) {
    const modal = await this.modalCtrl.create({
      component: ViewVideoPage,
      componentProps: { url: urls },
      cssClass: 'viewVideoModal'
    })
    return modal.present();
  }
  
  ngOnInit() {
  }

}
