import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TachesRealisesPage } from './taches-realises.page';

const routes: Routes = [
  {
    path: '',
    component: TachesRealisesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TachesRealisesPage]
})
export class TachesRealisesPageModule {}
