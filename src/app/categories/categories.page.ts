import { AngularFirestore } from '@angular/fire/firestore';
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActionSheetController, NavController, ToastController } from '@ionic/angular';
import { CategorieService } from '../services/categorie.service';
import { Categorie } from '../services/categorie';
import { trigger, state, style } from '@angular/animations';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: ['./categories.page.scss'],
  providers:[DatePipe],
  //for animation purpose
  animations: [
    trigger('itemState', [
      state('idle', style({
        opacity: '0.3',
        transform: 'scale(1)'
      })),
    ])
  ]
})
export class CategoriesPage implements OnInit {

  user: any;
  categories: any[];

  constructor(private categorieservice: CategorieService,
    private afs :AngularFirestore,
    public toastCtrl: ToastController,
    public actionsheetCtrl: ActionSheetController ,
    private navCtrl: NavController) {
    
   }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('userInfo'));
     //this.type = this.user.user.type;
      console.log(this.user);
    this.afs.collection('categories', ref => ref.where('user_id', '==', this.user.uid)).snapshotChanges().subscribe(rdvs => {

      this.categories = rdvs.map(item => {
       // console.log(item);
        let uid = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { uid, ...(data as {}) } as Categorie;
      });
      console.log(this.categories );
    });
    
  }
  async openMenu(task) {  
    const actionSheet = await this.actionsheetCtrl.create({  
      header: 'Modify your album',  
      buttons: [  
        {  
          text: 'Modifier',  
          role: 'destructive',  
          handler: () => {  
            localStorage.setItem('categorie', JSON.stringify(task));
    console.log(task);
    this.navCtrl.navigateForward(['edit-categorie']);
          }  
        },{  
          text: 'Supprimer',  
          handler: () => {  
            this.categorieservice.deletecategorie(task.uid);
            this.openToast();
          }  
        }, {  
          text: 'Fermer',  
          role: 'cancel',  
          handler: () => {  
            console.log('Cancel clicked');  
          }  
        }  
      ]  
    });  
    await actionSheet.present();  
  }   
  
  async openToast() {   
    const toast = await this.toastCtrl.create({  
      message: 'categorie supprimer ave succes',  
      animated: false,  
      duration: 8000,
        
      position: 'middle',  
    });  
    toast.present();  
    toast.onDidDismiss().then((val) => {  
      console.log('Toast Dismissed');   
    });  
  } 
  async modifier(categorie) {
    
    localStorage.setItem('categorie', JSON.stringify(categorie));
    console.log(categorie);
    this.navCtrl.navigateForward(['modifier-categorie']);
   
  }
  async openModal() {
    this.navCtrl.navigateForward("add-categorie");
    
  }

}
