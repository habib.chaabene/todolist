import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageEnfantsPage } from './image-enfants.page';

describe('ImageEnfantsPage', () => {
  let component: ImageEnfantsPage;
  let fixture: ComponentFixture<ImageEnfantsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageEnfantsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageEnfantsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
