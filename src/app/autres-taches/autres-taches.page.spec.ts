import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutresTachesPage } from './autres-taches.page';

describe('AutresTachesPage', () => {
  let component: AutresTachesPage;
  let fixture: ComponentFixture<AutresTachesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutresTachesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutresTachesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
