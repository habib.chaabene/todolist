import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncourPage } from './encour.page';

describe('EncourPage', () => {
  let component: EncourPage;
  let fixture: ComponentFixture<EncourPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EncourPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncourPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
