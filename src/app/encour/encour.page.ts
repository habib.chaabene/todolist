import { AngularFirestore } from '@angular/fire/firestore';
import { TaskService } from './../services/task.service';
import { Component, OnInit } from '@angular/core';
import { ActionSheetController, AlertController, NavController, ToastController } from '@ionic/angular';
import { Task } from '../services/task';
import { trigger, state, style } from '@angular/animations';
import { DatePipe } from '@angular/common';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

@Component({
  selector: 'app-encour',
  templateUrl: './encour.page.html',
  styleUrls: ['./encour.page.scss'],
  providers:[DatePipe],
  //for animation purpose
  animations: [
    trigger('itemState', [
      state('idle', style({
        opacity: '0.3',
        transform: 'scale(1)'
      })),
    ])
  ]
})
export class EncourPage implements OnInit {

  user: any;
  taches: any[];
  date: Date;
  today: string;
  hour: string;
  clickSub: any;

  constructor(private taskservice: TaskService,
    public toastCtrl: ToastController,    
    public alertController: AlertController,
    private datePipe: DatePipe,
    private localNotifications: LocalNotifications,
    public actionsheetCtrl: ActionSheetController ,
    private afs: AngularFirestore,
    private navCtrl: NavController) {
    
   }

  ngOnInit() {
    setInterval(() => {
      this.getnotif();
    }, 51000);
    this.user = JSON.parse(localStorage.getItem('userInfo'));
     //this.type = this.user.user.type;
      console.log(this.user);
    this.taskservice.getTasksongoing(this.user.uid).subscribe(rdvs => {

      this.taches = rdvs.map(item => {
        
        let id = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { id, ...(data as {}) } as Task;
      });
      console.log('taches',this.taches);
    });
    
  }

  getnotif(){
    this.date= new Date();
    this.today = this.datePipe.transform(this.date, 'yyyy-MM-dd');
    //this.hour = this.datePipe.transform(this.date, 'hh:mm');
    //let heure  = moment(this.hour).add(30, 'm').toDate();

    var d2 = new Date (this.date);
    d2.setMinutes ( this.date.getMinutes() + 30 );
    //console.log( d2);
    this.hour = this.datePipe.transform(d2, 'HH:mm');
    //console.log('today',this.hour);
    
    this.taskservice.getTodayTasks().subscribe(rdvs => {

      this.taches = rdvs.map(item => {
        //console.log(item);
        let id = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { id, ...(data as {}) } as Task;
      });
      console.log('taches',this.taches);
      this.taches.forEach(x=>{
        let heure = this.datePipe.transform(x.date_debut, 'HH:mm');
        console.log('haure',this.hour,heure);
        if(heure == this.hour)
          {
            console.log('haure',this.hour,heure);
            console.log('ok');
            let Notif={};
            Notif['user_id']= this.user.uid;
            let record = Object.assign(Notif, x);
            this.afs.collection('notification').add(record)
            this.multipleNotif(x);
          }
      })
    });
  }

  multipleNotif(x) {
    this.clickSub = this.localNotifications.on('click').subscribe(data => {
      console.log(data);
      // this.presentAlert('Your notifiations contains a secret = ' + data.data.secret);
      this.unsub();
    });
    this.localNotifications.schedule({
      title: x.nom,
      text: x.date_debut,
      actions: [
        { id: 'yes', title: 'Confirmer' },
        { id: 'no', title: 'Annuler' }
      ]
    });
  }
  unsub() {
    this.clickSub.unsubscribe();
  }

  async modifier(task) {
    
    localStorage.setItem('tache', JSON.stringify(task));
    console.log(task);
    this.navCtrl.navigateForward(['modifier-tache']);
    /*
    const modal = await this.modalController.create({
      component: AjouterTachePage,
      componentProps: {
        "paramID": 123,
        "paramTitle": "Test Title"
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        //alert('Modal Sent Data :'+ dataReturned);
      }
    });
 
    return await modal.present();
    */
  }
  async openModal() {
    this.navCtrl.navigateForward("ajouter-tache");
    /*
    const modal = await this.modalController.create({
      component: AjouterTachePage,
      componentProps: {
        "paramID": 123,
        "paramTitle": "Test Title"
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        //alert('Modal Sent Data :'+ dataReturned);
      }
    });
 
    return await modal.present();
    */
  }

  async openMenu(task) {  
    const actionSheet = await this.actionsheetCtrl.create({  
      header: 'Modify your album',  
      buttons: [  
        {  
          text: 'Modifier',  
          role: 'destructive',  
          handler: () => {  
            localStorage.setItem('tache', JSON.stringify(task));
    console.log(task);
    this.navCtrl.navigateForward(['modifier-tache']);
          }  
        },{  
          text: 'Supprimer',  
          handler: () => {  
            this.taskservice.deletetask(task.id);
            this.openToast();
          }  
        }, {  
          text: 'Fermer',  
          role: 'cancel',  
          handler: () => {  
            console.log('Cancel clicked');  
          }  
        }  
      ]  
    });  
    await actionSheet.present();  
  }   
  
  async openToast() {   
    const toast = await this.toastCtrl.create({  
      message: 'tache supprimer ave succes',  
      animated: false,  
      duration: 8000,
        
      position: 'middle',  
    });  
    toast.present();  
    toast.onDidDismiss().then((val) => {  
      console.log('Toast Dismissed');   
    });  
  } 

}
