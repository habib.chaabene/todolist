import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomeEnfantsPage } from './home-enfants.page';

const routes: Routes = [
  {
    path: '',
    component: HomeEnfantsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomeEnfantsPage]
})
export class HomeEnfantsPageModule {}
