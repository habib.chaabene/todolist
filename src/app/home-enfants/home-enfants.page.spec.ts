import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeEnfantsPage } from './home-enfants.page';

describe('HomeEnfantsPage', () => {
  let component: HomeEnfantsPage;
  let fixture: ComponentFixture<HomeEnfantsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeEnfantsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeEnfantsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
