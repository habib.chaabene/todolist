import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminerPage } from './terminer.page';

describe('TerminerPage', () => {
  let component: TerminerPage;
  let fixture: ComponentFixture<TerminerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerminerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerminerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
