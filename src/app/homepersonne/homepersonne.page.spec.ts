import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepersonnePage } from './homepersonne.page';

describe('HomepersonnePage', () => {
  let component: HomepersonnePage;
  let fixture: ComponentFixture<HomepersonnePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepersonnePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepersonnePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
