import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  user: any;

  constructor(private afs:AngularFirestore,
    private router: Router) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('userInfo'));
  }
  suppAccount(){
    this.afs.collection('users-enfants').doc(this.user.uid).delete();
    this.router.navigate(['/type-user']);
  }
  editAccount(){    
    this.router.navigate(['/modifier-profile']);
  }

}
