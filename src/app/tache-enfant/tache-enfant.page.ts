import { TacheEnfantService } from './../services/tache-enfant.service';
import { ActionSheetController, NavController, ToastController } from '@ionic/angular';
import { Task } from './../services/task';
import { AngularFirestore } from '@angular/fire/firestore';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tache-enfant',
  templateUrl: './tache-enfant.page.html',
  styleUrls: ['./tache-enfant.page.scss'],
})
export class TacheEnfantPage implements OnInit {
  categorie: string;
  enfant_id: string;
  projets: Task[];

  constructor(private firestore: AngularFirestore,
    public toastCtrl: ToastController,
    private tacheservice: TacheEnfantService,
    private navCtrl: NavController,
    public actionsheetCtrl: ActionSheetController ,) { }

  ngOnInit() {
    this.categorie = localStorage.getItem('categorie');
    console.log(this.categorie);
    this.enfant_id = localStorage.getItem('enfant_id');
    this.firestore.collection('taches-enfants', ref => ref.where('user_id', '==', this.enfant_id).where('categorie', '==', this.categorie)).snapshotChanges().subscribe(rdvs => {

      this.projets = rdvs.map(item => {
       // console.log(item);
        let id = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { id, ...(data as {}) } as Task;
      });
      console.log(this.projets );
    });
  }

  async openMenu(task) {  
    const actionSheet = await this.actionsheetCtrl.create({  
      header: '',  
      buttons: [  
        {  
          text: 'Modifier',  
          role: 'destructive',  
          handler: () => {  
            localStorage.setItem('tacheenfant', JSON.stringify(task));
    console.log(task);
    this.navCtrl.navigateForward(['modifier-tacheenfant']);
          }  
        },{  
          text: 'Supprimer',  
          handler: () => {  
            this.tacheservice.deletetache(task.id);
            this.openToast();
          }  
        }, {  
          text: 'Fermer',  
          role: 'cancel',  
          handler: () => {  
            console.log('Cancel clicked');  
          }  
        }  
      ]  
    });  
    await actionSheet.present();  
  }   
  
  async openToast() {   
    const toast = await this.toastCtrl.create({  
      message: 'تم حذف المهمة بنجاح',  
      animated: false,  
      duration: 8000,
        
      position: 'middle',  
    });  
    toast.present();  
    toast.onDidDismiss().then((val) => {  
      console.log('Toast Dismissed');   
    });  
  } 
  async modifier(projet) {
    
    localStorage.setItem('tacheenfant', JSON.stringify(projet));
    console.log(projet);
    this.navCtrl.navigateForward(['modifier-tacheenfant']);
   
  }

}
