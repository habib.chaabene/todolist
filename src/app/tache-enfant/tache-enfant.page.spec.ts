import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TacheEnfantPage } from './tache-enfant.page';

describe('TacheEnfantPage', () => {
  let component: TacheEnfantPage;
  let fixture: ComponentFixture<TacheEnfantPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TacheEnfantPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TacheEnfantPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
