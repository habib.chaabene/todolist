import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TacheEnfantPage } from './tache-enfant.page';

const routes: Routes = [
  {
    path: '',
    component: TacheEnfantPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TacheEnfantPage]
})
export class TacheEnfantPageModule {}
