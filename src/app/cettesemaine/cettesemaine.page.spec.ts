import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CettesemainePage } from './cettesemaine.page';

describe('CettesemainePage', () => {
  let component: CettesemainePage;
  let fixture: ComponentFixture<CettesemainePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CettesemainePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CettesemainePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
