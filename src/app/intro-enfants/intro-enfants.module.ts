import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { IntroEnfantsPage } from './intro-enfants.page';

const routes: Routes = [
  {
    path: '',
    component: IntroEnfantsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [IntroEnfantsPage]
})
export class IntroEnfantsPageModule {}
