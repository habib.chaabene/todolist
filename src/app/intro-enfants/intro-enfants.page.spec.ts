import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroEnfantsPage } from './intro-enfants.page';

describe('IntroEnfantsPage', () => {
  let component: IntroEnfantsPage;
  let fixture: ComponentFixture<IntroEnfantsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroEnfantsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroEnfantsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
