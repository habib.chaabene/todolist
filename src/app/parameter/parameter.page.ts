import { Router } from "@angular/router";
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-parameter',
  templateUrl: './parameter.page.html',
  styleUrls: ['./parameter.page.scss'],
})
export class ParameterPage implements OnInit {

  constructor(
    private authService: AuthService,
    private router:Router
  ) { }

  ngOnInit() {
  }
  
  Deconnexion(){
    this.authService.logout();
  }

}
