import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-compte',
  templateUrl: './compte.page.html',
  styleUrls: ['./compte.page.scss'],
})
export class ComptePage implements OnInit {
  user: any;

  constructor(private afs:AngularFirestore,
    private router: Router) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('userInfo'));
  }

  suppAccount(){
    this.afs.collection('users').doc(this.user.uid).delete();
    this.router.navigate(['/type-user']);
  }
  editAccount(){    
    this.router.navigate(['/modifier-compte']);
  }

}
