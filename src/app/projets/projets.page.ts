import { AngularFirestore } from '@angular/fire/firestore';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActionSheetController, NavController, ToastController } from '@ionic/angular';
import { ProjetService } from '../services/projet.service';
import { Projet } from '../services/projet';
import { trigger, state, style } from '@angular/animations';
import { TaskService } from '../services/task.service';
import { Task } from '../services/task';

@Component({
  selector: 'app-projets',
  templateUrl: './projets.page.html',
  styleUrls: ['./projets.page.scss'],
  providers:[DatePipe],
  //for animation purpose
  animations: [
    trigger('itemState', [
      state('idle', style({
        opacity: '0.3',
        transform: 'scale(1)'
      })),
    ])
  ]
})
export class ProjetsPage implements OnInit {

  user: any;
  projets: any[];
  taches: Task[];
  nbr: number;
  progression: number;
  date: Date;
  today: string;
  hour: string;
  clickSub: any;

  constructor(private projetservice: ProjetService,
    public toastCtrl: ToastController,
    private datePipe: DatePipe,
    public actionsheetCtrl: ActionSheetController ,
    private taskservice: TaskService,
    private localNotifications: LocalNotifications,
    private navCtrl: NavController,
    private afs: AngularFirestore) {
    
   }

  ngOnInit() {
    setInterval(() => {
      this.getnotif();
    }, 51000);
    this.user = JSON.parse(localStorage.getItem('userInfo'));
     //this.type = this.user.user.type;
      console.log(this.user);
    this.projetservice.getProjets().subscribe(rdvs => {

      this.projets = rdvs.map(item => {
       // console.log(item);
        let id = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { id, ...(data as {}) } as Projet;
      });
      console.log(this.projets );
      this.projets.forEach(x=>{
        this.getprog(x.id);
      })
    });
    
    
  }

  
  getnotif(){
    this.date= new Date();
    this.today = this.datePipe.transform(this.date, 'yyyy-MM-dd');
    //this.hour = this.datePipe.transform(this.date, 'hh:mm');
    //let heure  = moment(this.hour).add(30, 'm').toDate();

    var d2 = new Date (this.date);
    d2.setMinutes ( this.date.getMinutes() + 30 );
    //console.log( d2);
    this.hour = this.datePipe.transform(d2, 'HH:mm');
    //console.log('today',this.hour);
    
    this.taskservice.getTodayTasks().subscribe(rdvs => {

      this.taches = rdvs.map(item => {
        //console.log(item);
        let id = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { id, ...(data as {}) } as Task;
      });
      console.log('taches',this.taches);
      this.taches.forEach(x=>{
        let heure = this.datePipe.transform(x.date_debut, 'HH:mm');
        console.log('haure',this.hour,heure);
        if(heure == this.hour)
          {
            console.log('haure',this.hour,heure);
            console.log('ok');
            let Notif={};
            Notif['user_id']= this.user.uid;
            let record = Object.assign(Notif, x);
            this.afs.collection('notification').add(record)
            this.multipleNotif(x);
          }
      })
    });
  }

  multipleNotif(x) {
    this.clickSub = this.localNotifications.on('click').subscribe(data => {
      console.log(data);
      // this.presentAlert('Your notifiations contains a secret = ' + data.data.secret);
      this.unsub();
    });
    this.localNotifications.schedule({
      title: x.nom,
      text: x.date_debut,
      actions: [
        { id: 'yes', title: 'Confirmer' },
        { id: 'no', title: 'Annuler' }
      ]
    });
  }
  unsub() {
    this.clickSub.unsubscribe();
  }

  getprog(id){
    this.taskservice.getTasksbyprojet(id).subscribe(rdvs => {

      this.taches = rdvs.map(item => {
        console.log(item);
        let id = item.payload.doc.id;
        let data = item.payload.doc.data();
        return { id, ...(data as {}) } as Task;
      });
      let i =0;
      this.taches.forEach(x=>{
        if(x.etat == 'terminer')
        i++;
      })
      console.log(this.taches.length,i);
      this.nbr = this.taches.length;
      this.progression = i/ this.nbr;
    });
  }
  async openMenu(task) {  
    const actionSheet = await this.actionsheetCtrl.create({  
      header: '',  
      buttons: [  
        {  
          text: 'Modifier',  
          role: 'destructive',  
          handler: () => {  
            localStorage.setItem('projet', JSON.stringify(task));
    console.log(task);
    this.navCtrl.navigateForward(['modifier-projet']);
          }  
        },{  
          text: 'Supprimer',  
          handler: () => {  
            this.projetservice.deleteprojet(task.id);
            this.openToast();
          }  
        }, {  
          text: 'Fermer',  
          role: 'cancel',  
          handler: () => {  
            console.log('Cancel clicked');  
          }  
        }  
      ]  
    });  
    await actionSheet.present();  
  }   
  
  async openToast() {   
    const toast = await this.toastCtrl.create({  
      message: 'projet supprimer ave succes',  
      animated: false,  
      duration: 8000,
        
      position: 'middle',  
    });  
    toast.present();  
    toast.onDidDismiss().then((val) => {  
      console.log('Toast Dismissed');   
    });  
  } 
  async modifier(projet) {
    
    localStorage.setItem('projet', JSON.stringify(projet));
    console.log(projet);
    this.navCtrl.navigateForward(['modifier-projet']);
   
  }
  async openModal() {
    this.navCtrl.navigateForward("ajouter-projet");
    
  }

}
