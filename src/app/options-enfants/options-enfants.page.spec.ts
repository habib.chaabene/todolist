import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionsEnfantsPage } from './options-enfants.page';

describe('OptionsEnfantsPage', () => {
  let component: OptionsEnfantsPage;
  let fixture: ComponentFixture<OptionsEnfantsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionsEnfantsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionsEnfantsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
