import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeUserPage } from './type-user.page';

describe('TypeUserPage', () => {
  let component: TypeUserPage;
  let fixture: ComponentFixture<TypeUserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeUserPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
