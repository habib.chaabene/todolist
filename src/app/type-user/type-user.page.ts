import { MenuController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-type-user',
  templateUrl: './type-user.page.html',
  styleUrls: ['./type-user.page.scss'],
})
export class TypeUserPage implements OnInit {

  constructor(public menuCtrl: MenuController,) { }

  ngOnInit() {
    this.menuCtrl.enable(false);
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }
}
