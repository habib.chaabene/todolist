import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterProjetPage } from './ajouter-projet.page';
import { ReactiveFormsModule } from '@angular/forms';

describe('AjouterProjetPage', () => {
  let component: AjouterProjetPage;
  let fixture: ComponentFixture<AjouterProjetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterProjetPage ],      
      imports: [ReactiveFormsModule],  
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterProjetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
