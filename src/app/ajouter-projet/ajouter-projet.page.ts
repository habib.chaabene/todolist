import { Component, LOCALE_ID, OnInit } from '@angular/core';
import { MbscModule, mobiscroll } from '@mobiscroll/angular-lite';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ModalController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ProjetService } from '../services/projet.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { DatePipe, registerLocaleData } from '@angular/common';

mobiscroll.settings = {
  theme: 'ios',
  themeVariant: 'light',
  display: 'bubble'
};
const now = new Date();
import localeFr from '@angular/common/locales/fr';

registerLocaleData(localeFr);

@Component({
  selector: 'app-ajouter-projet',
  templateUrl: './ajouter-projet.page.html',
  styleUrls: ['./ajouter-projet.page.scss'],
  providers: [DatePipe,{ provide: LOCALE_ID, useValue: "fr-FR" }]
})
export class AjouterProjetPage implements OnInit {

  modalTitle: string;
  modelId: number;
  external = now;
  proForm: FormGroup;
  progression: number;
  user: any;
  date: Date;
  today: string;
  min: string;
 
  constructor(
    private modalController: ModalController,
    public fb: FormBuilder,
    private socialSharing: SocialSharing,
    private router: Router,
    private datePipe: DatePipe,
    private projetService:ProjetService,
    private toastCtrl:ToastController
  ) { }
    
  externalSettings: MbscModule  = {
    controls: ['calendar', 'time'],
    showOnTap: false,
    showOnFocus: false
};

  ngOnInit() {
    this.date = new Date();
    let maxDate: any = new Date(new Date().setFullYear(new Date().getFullYear() + 2)).toISOString();
    this.today = this.datePipe.transform(this.date, 'yyyy-MM-dd');

    this.user = JSON.parse(localStorage.getItem('userInfo'));
     //this.type = this.user.user.type;
      console.log(this.today);
    this.proForm = this.fb.group({
      titre: [''],
      description: [''],
      progression: ['0'],
      date_debut: [''],
      date_fin: [''],
      user_id: [this.user.uid],
    })
    
  }
  formSubmit() {
    if (!this.proForm.valid) {
      return false;
    } else {
      //this.proForm.progression = 0;
      console.log(this.proForm.value);
       this.progression = 0;
      this.projetService.createProjet(this.proForm.value).then(res => {
        console.log(res);
        this.proForm.reset();
        this.openToast();
        this.router.navigate(['/projets']);
      },error => console.log(error));
       // .catch(error => console.log(error));
       
    }
  }
  async openToast() {   
    const toast = await this.toastCtrl.create({  
      message: 'projet ajouté avec succés ',  
      animated: false,  
      duration: 8000,
        
      position: 'middle',  
    });  
    toast.present();  
    toast.onDidDismiss().then((val) => {  
      console.log('Toast Dismissed');   
    });  
  } 

 
 
  async closeModal() {
    this.router.navigate(['/projets']);
  }

  change(ev){
    this.min = this.datePipe.transform(ev.detail.value, 'yyyy-mm-dd');
    console.log('min',this.min);
    console.log(ev.detail.value);
  }

  minDate: Date = new Date(2017, 11, 1);
    maxDate: Date = new Date(2017, 11, 10);

    myDateOptions: MbscModule = {
        display: 'top',
        theme: 'ios',
        max: new Date(),
        min: new Date(1960, 0, 1)
    };

    birthday: Date;
    appointment: Array<Date> = [];

}
