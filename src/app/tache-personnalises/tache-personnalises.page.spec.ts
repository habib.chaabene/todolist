import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TachePersonnalisesPage } from './tache-personnalises.page';

describe('TachePersonnalisesPage', () => {
  let component: TachePersonnalisesPage;
  let fixture: ComponentFixture<TachePersonnalisesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TachePersonnalisesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TachePersonnalisesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
