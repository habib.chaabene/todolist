import { Component, LOCALE_ID, OnInit } from '@angular/core';
import { DatePipe, registerLocaleData } from '@angular/common';
import { Router } from '@angular/router';
import { CalendarModalOptions } from 'ion2-calendar';
import localeFr from '@angular/common/locales/fr';
import { AngularFirestore } from '@angular/fire/firestore';
import { Task } from '../services/task';
import { trigger, state, style } from '@angular/animations';
import { ActionSheetController, NavController, ToastController } from '@ionic/angular';
import { TaskService } from '../services/task.service';
registerLocaleData(localeFr);

@Component({
  selector: 'app-calandar-enffant',
  templateUrl: './calandar-enffant.page.html',
  styleUrls: ['./calandar-enffant.page.scss'],
  providers:[DatePipe],
  //for animation purpose
  animations: [
    trigger('itemState', [
      state('idle', style({
        opacity: '0.3',
        transform: 'scale(1)'
      })),
    ])
  ]
})
export class CalandarEnffantPage implements OnInit {

  date: string;
  type: 'string';

  optionsMulti:CalendarModalOptions = {
    monthFormat: 'MMM YYYY',
    weekdays: ['Lu', 'Ma', 'Me', 'Je', 'Ve', 'sa', 'di'],
    weekStart: 0,
    defaultDate: new Date()
  };z
  nbr: any;
  taches: Task[];

  constructor(private taskservice: TaskService,
    public actionsheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    private datePipe: DatePipe,
    private navCtrl: NavController,
    public router:Router,
    private afs:AngularFirestore
    ) { }

    onChange($event) {
      console.log($event._d);
      let date = $event._d;
      date = this.datePipe.transform(date, 'yyyy-M-d');
      console.log(date);
      this.verif(date);
      localStorage.setItem('days',date);
      //this.router.navigate(['/day-tache']);
    }
    verif(date){
      this.afs.collection('taches-enfants', ref => ref.where('date_debut', '==', date)).snapshotChanges().subscribe(rdvs => {
  
        this.taches = rdvs.map(item => {
          //console.log(item);
          let id = item.payload.doc.id;
          let data = item.payload.doc.data();
          return { id, ...(data as {}) } as Task;
        });
        this.nbr = this.taches.length;
        console.log(this.nbr);
      });
    }
  test(ev: Date) {
    console.log(ev);
  }

  ngOnInit() {
  }

  async openMenu(task) {  
    const actionSheet = await this.actionsheetCtrl.create({  
      header: 'Modify your album',  
      buttons: [  
        {  
          text: 'Modifier',  
          role: 'destructive',  
          handler: () => {  
            localStorage.setItem('tache', JSON.stringify(task));
    console.log(task);
    this.navCtrl.navigateForward(['modifier-tache']);
          }  
        },{  
          text: 'Supprimer',  
          handler: () => {  
            this.taskservice.deletetask(task.id);
            this.openToast();
          }  
        }, {  
          text: 'Fermer',  
          role: 'cancel',  
          handler: () => {  
            console.log('Cancel clicked');  
          }  
        }  
      ]  
    });  
    await actionSheet.present();  
  }   
  
  async openToast() {   
    const toast = await this.toastCtrl.create({  
      message: 'tache supprimer ave succes',  
      animated: false,  
      duration: 8000,
        
      position: 'middle',  
    });  
    toast.present();  
    toast.onDidDismiss().then((val) => {  
      console.log('Toast Dismissed');   
    });  
  } 
}
