import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalandarEnffantPage } from './calandar-enffant.page';

describe('CalandarEnffantPage', () => {
  let component: CalandarEnffantPage;
  let fixture: ComponentFixture<CalandarEnffantPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalandarEnffantPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalandarEnffantPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
